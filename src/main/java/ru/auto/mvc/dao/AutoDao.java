package ru.auto.mvc.dao;


import ru.auto.mvc.form.AutoForm;
import ru.auto.mvc.model.Auto;

import java.util.List;

public interface AutoDao extends AbstractDao<Auto, Long> {
    List<Auto> search(Auto auto, Double beginCapacity, Double endCapacity);
}
