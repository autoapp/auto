package ru.auto.mvc.dao;


import ru.auto.mvc.model.Review;

import java.util.List;

public interface ReviewDao extends AbstractDao<Review, Long> {
    public List<Review> findAllById(Long id);

    public Integer plus();
}
