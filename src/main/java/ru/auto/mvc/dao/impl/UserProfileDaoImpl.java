package ru.auto.mvc.dao.impl;

import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.UserProfileDao;
import ru.auto.mvc.model.UserProfile;

@Repository
public class UserProfileDaoImpl extends AbstractDaoImpl<UserProfile, Long> implements UserProfileDao {

    protected UserProfileDaoImpl() {
        super(UserProfile.class);
    }

}
