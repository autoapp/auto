package ru.auto.mvc.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.UserDao;
import ru.auto.mvc.model.User;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, Long> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findByName(String name) {
        return (User) getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("login", name))
                .uniqueResult();
    }
}
