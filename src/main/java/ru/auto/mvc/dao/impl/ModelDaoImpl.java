package ru.auto.mvc.dao.impl;

import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.AbstractDao;
import ru.auto.mvc.dao.ModelDao;
import ru.auto.mvc.model.Model;

/**
 * Created by EduardL on 30.04.15.
 */
@Repository
public class ModelDaoImpl extends AbstractDaoImpl<Model, Long> implements ModelDao{

    protected ModelDaoImpl() {
        super(Model.class);
    }
}
