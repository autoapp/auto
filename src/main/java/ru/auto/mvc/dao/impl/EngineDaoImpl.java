package ru.auto.mvc.dao.impl;

import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.EngineDao;
import ru.auto.mvc.model.Engine;

/**
 * Created by EduardL on 02.05.15.
 */
@Repository
public class EngineDaoImpl extends AbstractDaoImpl<Engine, Long> implements EngineDao {

    protected EngineDaoImpl() {
        super(Engine.class);
    }
}
