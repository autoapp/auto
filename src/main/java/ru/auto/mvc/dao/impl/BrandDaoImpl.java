package ru.auto.mvc.dao.impl;

import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.BrandDao;
import ru.auto.mvc.model.Brand;

/**
 * Created by EduardL on 30.04.15.
 */

@Repository
public class BrandDaoImpl extends AbstractDaoImpl<Brand, Long> implements BrandDao {

    protected BrandDaoImpl() {
        super(Brand.class);
    }
}
