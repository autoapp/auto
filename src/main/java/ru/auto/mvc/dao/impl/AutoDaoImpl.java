package ru.auto.mvc.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.AutoDao;
import ru.auto.mvc.model.Auto;
import ru.auto.mvc.model.enums.*;

import java.util.List;

@Repository
public class AutoDaoImpl extends AbstractDaoImpl<Auto, Long> implements AutoDao {

    protected AutoDaoImpl() {
        super(Auto.class);
    }


//    @Override
//    public List<Auto> search(Auto auto) {
//        return (List<Auto>) getCurrentSession().createCriteria(Auto.class)
//                .add(Restrictions.eq("brand.brand", auto.getBrand().getBrand())).list();
//    }

    @Override
    public List<Auto> search(Auto auto, Double beginCapacity, Double endCapacity) {
        System.out.println("587");

        Long idBrand = auto.getBrand().getId();
        Long idModel = auto.getModel().getId();
        Long idGeneration = auto.getGeneration().getId();
        int beginYear = auto.getGeneration().getBeginYear();
        int endYear = auto.getGeneration().getEndYear();
        TypeOfEngine typeOfEngine = auto.getEngine().getTypeOfEngine();
        Long idEngine = auto.getEngine().getId();
        Fuel fuel = auto.getEngine().getFuel();
        Drivetrain drivetrain = auto.getDrivetrain();
        Transmission transmission = auto.getTransmission();
        Body body = auto.getBody();

        Criteria criteria = getCurrentSession().createCriteria(Auto.class);

        if (idBrand != 0) {
            criteria.add(Restrictions.eq("brand.id", idBrand));
        }
        if (idModel != 0) {
            criteria.add(Restrictions.eq("model.id", idModel));
        }
        if (idGeneration != 0) {
            criteria.add(Restrictions.eq("generation.id", idGeneration));
        }

        if (beginYear != 0 || endYear != 0) {
            criteria.createAlias("generation", "gen");

//            in base 2000-2005, in query 1990-2007
            Criterion criterion1 = Restrictions.and(Restrictions.ge("gen.beginYear", beginYear), Restrictions.le("gen.endYear", endYear));
//            in base 2000-2005, in query 2001-2004
            Criterion criterion2 = Restrictions.and(Restrictions.le("gen.beginYear", beginYear), Restrictions.ge("gen.endYear", endYear));
//            in base 2000-2005,in query 1990-2004
            Criterion criterion3 = Restrictions.and(Restrictions.ge("gen.beginYear", beginYear), Restrictions.ge("gen.endYear", endYear),
                    Restrictions.le("gen.beginYear", endYear));
//            in base 2000-2005, in query 2001-2008
            Criterion criterion4 = Restrictions.and(Restrictions.le("gen.beginYear", beginYear), Restrictions.le("gen.endYear", endYear),
                    Restrictions.ge("gen.endYear", beginYear));
            criteria.add(Restrictions.or(criterion1, criterion2, criterion3, criterion4));

        }

        if (typeOfEngine != null || fuel != null || beginCapacity != 0 || endCapacity != 0) {
            criteria.createAlias("engine", "eng");

            if (typeOfEngine != null) {
                System.out.println("type" + typeOfEngine);
                criteria.add(Restrictions.eq("eng.typeOfEngine", typeOfEngine));
            }
            if (fuel != null) {
                criteria.add(Restrictions.eq("eng.fuel", fuel));
            }
            if (beginCapacity != 0) {
                criteria.add(Restrictions.ge("eng.capacity", beginCapacity));
            }
            if (endCapacity != 0) {
                criteria.add(Restrictions.le("eng.capacity", endCapacity));
            }
        }
        if (idEngine != 0) {
            System.out.println("Engine DAO");
            criteria.add(Restrictions.eq("engine.id", idEngine));
        }

        if (drivetrain != null) {
            criteria.add(Restrictions.eq("drivetrain", drivetrain));
        }
        if (transmission != null) {
            criteria.add(Restrictions.eq("transmission", transmission));
        }
        if (body != null) {
            criteria.add(Restrictions.eq("body", body));
        }

        return (List<Auto>) criteria
                .list();
    }
}
