package ru.auto.mvc.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.ReviewDao;
import ru.auto.mvc.model.Review;

import java.util.List;

@Repository
public class ReviewDaoImpl extends AbstractDaoImpl<Review, Long> implements ReviewDao {

    protected ReviewDaoImpl() {
        super(Review.class);
    }

    @Override
    public List<Review> findAllById(Long id) {
        return (List<Review>) getCurrentSession().createCriteria(Review.class)
                .add(Restrictions.eq("auto.id", id)).list();
    }

    @Override
    public Integer plus() {
        return null;
    }
}
