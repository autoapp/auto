package ru.auto.mvc.dao.impl;

import org.springframework.stereotype.Repository;
import ru.auto.mvc.dao.GenerationDao;
import ru.auto.mvc.model.Generation;

/**
 * Created by EduardL on 01.05.15.
 */
@Repository
public class GenerationDaoImpl extends AbstractDaoImpl<Generation, Long> implements GenerationDao {
    protected GenerationDaoImpl() {
        super(Generation.class);
    }
}
