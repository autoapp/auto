package ru.auto.mvc.dao;


import ru.auto.mvc.model.User;

public interface UserDao extends AbstractDao<User, Long> {

    User findByName(String name);
}
