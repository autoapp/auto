package ru.auto.mvc.dao;

import ru.auto.mvc.model.Brand;

/**
 * Created by EduardL on 30.04.15.
 */
public interface BrandDao extends AbstractDao<Brand, Long> {
}
