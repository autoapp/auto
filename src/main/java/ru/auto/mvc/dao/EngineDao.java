package ru.auto.mvc.dao;

import ru.auto.mvc.model.Engine;

/**
 * Created by EduardL on 02.05.15.
 */
public interface EngineDao extends AbstractDao<Engine, Long> {
}
