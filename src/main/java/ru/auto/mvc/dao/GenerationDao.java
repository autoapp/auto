package ru.auto.mvc.dao;

import ru.auto.mvc.model.Generation;

/**
 * Created by EduardL on 01.05.15.
 */
public interface GenerationDao extends AbstractDao<Generation, Long> {
}
