package ru.auto.mvc.dao;


import ru.auto.mvc.model.UserProfile;

public interface UserProfileDao extends AbstractDao<UserProfile, Long> {

}
