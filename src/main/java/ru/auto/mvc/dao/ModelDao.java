package ru.auto.mvc.dao;

import ru.auto.mvc.model.Model;

/**
 * Created by EduardL on 30.04.15.
 */
public interface ModelDao extends AbstractDao<Model, Long> {
}
