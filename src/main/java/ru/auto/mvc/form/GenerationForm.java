package ru.auto.mvc.form;

/**
 * Created by EduardL on 24.04.15.
 */
public class GenerationForm {
    private Long id;
    private String generation;
    private Integer beginYear;
    private Integer endYear;
    private ModelForm model;

    public GenerationForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Integer getBeginYear() {
        return beginYear;
    }

    public void setBeginYear(int beginYear) {
        this.beginYear = beginYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public ModelForm getModel() {
        return model;
    }

    public void setModel(ModelForm model) {
        this.model = model;
    }
}
