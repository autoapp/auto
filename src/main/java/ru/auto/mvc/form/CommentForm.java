package ru.auto.mvc.form;

import java.util.Date;
import java.util.List;

/**
 * Created by Rigen on 23.04.15.
 */
public class CommentForm {
    private Long id;
    private Long userId;
    private Long reviewId;
    private Long commentId;
    private String text;
    private Integer plus;
    private Integer minus;
    private Date date;
    private List<CommentForm> commentList;

    public CommentForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPlus() {
        return plus;
    }

    public void setPlus(Integer plus) {
        this.plus = plus;
    }

    public Integer getMinus() {
        return minus;
    }

    public void setMinus(Integer minus) {
        this.minus = minus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<CommentForm> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<CommentForm> commentList) {
        this.commentList = commentList;
    }
}
