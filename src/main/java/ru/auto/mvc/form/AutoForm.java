package ru.auto.mvc.form;

import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
public class AutoForm {
    private Long id;
    private BrandForm brand;
    private ModelForm model;
    private String drivetrain;
    private GenerationForm generation;
    private List<ImageForm> images;
    private String transmission;
    private EngineForm engine;
    private String body;
    private Integer euroNCAP;

    public AutoForm() {
    }

    public Integer getEuroNCAP() {
        return euroNCAP;
    }

    public void setEuroNCAP(Integer euroNCAP) {
        this.euroNCAP = euroNCAP;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BrandForm getBrand() {
        return brand;
    }

    public void setBrand(BrandForm brand) {
        this.brand = brand;
    }

    public ModelForm getModel() {
        return model;
    }

    public void setModel(ModelForm model) {
        this.model = model;
    }

    public String getDrivetrain() {
        return drivetrain;
    }

    public void setDrivetrain(String drivetrain) {
        this.drivetrain = drivetrain;
    }

    public GenerationForm getGeneration() {
        return generation;
    }

    public void setGeneration(GenerationForm generation) {
        this.generation = generation;
    }

    public List<ImageForm> getImages() {
        return images;
    }

    public void setImages(List<ImageForm> images) {
        this.images = images;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public EngineForm getEngine() {
        return engine;
    }

    public void setEngine(EngineForm engine) {
        this.engine = engine;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
