package ru.auto.mvc.form;

/**
 * Created by EduardL on 24.04.15.
 */
public class BrandForm {
    private Long id;
    private String brand;

    public BrandForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
