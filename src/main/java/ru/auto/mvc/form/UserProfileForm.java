package ru.auto.mvc.form;

/**
 * Created by Rigen on 22.04.15.
 */
public class UserProfileForm {
    private Long id;
    private String name;
    private String surname;
    private String gender;
    private Boolean emailAccess;
    private String location;
    private String summary;
    private String birthDate;
    private String regDate;

    public UserProfileForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser() {
        return id;
    }

    public void setUser(Long user) {
        this.id = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getEmailAccess() {
        return emailAccess;
    }

    public void setEmailAccess(Boolean emailAccess) {
        this.emailAccess = emailAccess;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
}
