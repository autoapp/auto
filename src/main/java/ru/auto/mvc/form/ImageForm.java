package ru.auto.mvc.form;

import ru.auto.mvc.form.viewForm.UserViewForm;

/**
 * Created by Rigen on 22.04.15.
 */
public class ImageForm {
    private Long id;
    private UserViewForm user;
    private String link;

    public ImageForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserViewForm getUser() {
        return user;
    }

    public void setUser(UserViewForm user) {
        this.user = user;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
