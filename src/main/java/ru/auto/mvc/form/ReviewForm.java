package ru.auto.mvc.form;

import ru.auto.mvc.form.viewForm.UserViewForm;

/**
 * Created by Rigen on 23.04.15.
 */
public class ReviewForm {
    private Long id;
    private UserViewForm user;
    private Long autoId;
    private String pros;
    private String cons;
    private String impression;
    private String updateName;
    private String updateText;
    private String date;
    private Integer plus;
    private Integer minus;
    private Double rate;
    private Integer expRate;
    private String motorText;
    private Integer motorRate;
    private String gearboxText;
    private Integer gearboxRate;
    private String electronicText;
    private Integer electronicRate;
    private String carcassText;
    private Integer carcassRate;
    private String suspentionText;
    private Integer suspentionRate;
    private Integer mileage;
    private Integer year;
    private Integer experience;

    public ReviewForm() {
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserViewForm getUser() {
        return user;
    }

    public void setUser(UserViewForm user) {
        this.user = user;
    }

    public Long getAutoId() {
        return autoId;
    }

    public void setAutoId(Long autoId) {
        this.autoId = autoId;
    }

    public String getPros() {
        return pros;
    }

    public void setPros(String pros) {
        this.pros = pros;
    }

    public String getCons() {
        return cons;
    }

    public void setCons(String cons) {
        this.cons = cons;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateText() {
        return updateText;
    }

    public void setUpdateText(String updateText) {
        this.updateText = updateText;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPlus() {
        return plus;
    }

    public void setPlus(Integer plus) {
        this.plus = plus;
    }

    public Integer getMinus() {
        return minus;
    }

    public void setMinus(Integer minus) {
        this.minus = minus;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getExpRate() {
        return expRate;
    }

    public void setExpRate(Integer expRate) {
        this.expRate = expRate;
    }

    public String getMotorText() {
        return motorText;
    }

    public void setMotorText(String motorText) {
        this.motorText = motorText;
    }

    public Integer getMotorRate() {
        return motorRate;
    }

    public void setMotorRate(Integer motorRate) {
        this.motorRate = motorRate;
    }

    public String getGearboxText() {
        return gearboxText;
    }

    public void setGearboxText(String gearboxText) {
        this.gearboxText = gearboxText;
    }

    public Integer getGearboxRate() {
        return gearboxRate;
    }

    public void setGearboxRate(Integer gearboxRate) {
        this.gearboxRate = gearboxRate;
    }

    public String getElectronicText() {
        return electronicText;
    }

    public void setElectronicText(String electronicText) {
        this.electronicText = electronicText;
    }

    public Integer getElectronicRate() {
        return electronicRate;
    }

    public void setElectronicRate(Integer electronicRate) {
        this.electronicRate = electronicRate;
    }

    public String getCarcassText() {
        return carcassText;
    }

    public void setCarcassText(String carcassText) {
        this.carcassText = carcassText;
    }

    public Integer getCarcassRate() {
        return carcassRate;
    }

    public void setCarcassRate(Integer carcassRate) {
        this.carcassRate = carcassRate;
    }

    public String getSuspentionText() {
        return suspentionText;
    }

    public void setSuspentionText(String suspentionText) {
        this.suspentionText = suspentionText;
    }

    public Integer getSuspentionRate() {
        return suspentionRate;
    }

    public void setSuspentionRate(Integer suspentionRate) {
        this.suspentionRate = suspentionRate;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }
}
