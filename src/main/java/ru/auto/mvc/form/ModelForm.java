package ru.auto.mvc.form;

import ru.auto.mvc.model.Generation;

import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
public class ModelForm {
    private Long idModel;
    private String model;
    private BrandForm brand;

    public Long getIdModel() {
        return idModel;
    }

    public void setIdModel(Long idModel) {
        this.idModel = idModel;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BrandForm getBrand() {
        return brand;
    }

    public void setBrand(BrandForm brand) {
        this.brand = brand;
    }

}
