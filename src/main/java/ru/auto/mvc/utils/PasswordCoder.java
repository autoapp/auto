package ru.auto.mvc.utils;

import java.security.MessageDigest;
import java.util.Random;

/**
 * Created by Rigen on 09.04.15.
 */

public class PasswordCoder {

    private static final int SALT_LENGTH_DEFAULT = 5;

    /**
     * MD5 hashing
     */

    public static String md5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            return byteArrayToHexString(byteData);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String md5WithSalt(String password) {
        return md5WithSalt(password, SALT_LENGTH_DEFAULT);
    }

    public static String md5WithSalt(String password, String salt) {
        String passwordWithSalt = mergePassSalt(password, salt);
        return md5(passwordWithSalt);
    }

    public static String md5WithSalt(String password, int saltLength) {
        String salt = generateSalt(saltLength);
        return md5WithSalt(password, salt);
    }

    public static String md5WithSaltAndParameter(String password, int saltLength, String localParameter) {
        String salt = generateSalt(saltLength);
        return md5WithSalt(password, salt + localParameter);
    }

    public static String md5WithSaltAndParameter(String password, String localParameter) {
        String salt = generateSalt(5);
        return md5WithSalt(password, salt + localParameter);
    }

    public static String md5WithSaltAndParameter(String password, String salt, String localParameter) {
        return md5WithSalt(password, salt + localParameter);
    }

    /**
     * SHA-1 hashing
     */

    public static String sha1(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            return byteArrayToHexString(byteData);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String sha1WithSalt(String password) {
        return sha1WithSalt(password, SALT_LENGTH_DEFAULT);
    }

    public static String sha1WithSalt(String password, String salt) {
        String passwordWithSalt = mergePassSalt(password, salt);
        return sha1(passwordWithSalt);
    }

    public static String sha1WithSalt(String password, int saltLength) {
        String salt = generateSalt(saltLength);
        return sha1WithSalt(password, salt);
    }

    public static String sha1WithSaltAndParameter(String password, int saltLength, String localParameter) {
        String salt = generateSalt(saltLength);
        return sha1WithSalt(password, salt + localParameter);
    }

    public static String sha1WithSaltAndParameter(String password, String localParameter) {
        String salt = generateSalt(5);
        return sha1WithSalt(password, salt + localParameter);
    }

    public static String sha1WithSaltAndParameter(String password, String salt, String localParameter) {
        return sha1WithSalt(password, salt + localParameter);
    }

    /**
     * SHA-256 hashing
     */

    public static String sha256(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            return byteArrayToHexString(byteData);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String sha256WithSalt(String password) {
        return sha256WithSalt(password, SALT_LENGTH_DEFAULT);
    }

    public static String sha256WithSalt(String password, String salt) {
        String passwordWithSalt = mergePassSalt(password, salt);
        return sha256(passwordWithSalt);
    }

    public static String sha256WithSalt(String password, int saltLength) {
        String salt = generateSalt(saltLength);
        return sha256WithSalt(password, salt);
    }

    public static String sha256WithSaltAndParameter(String password, int saltLength, String localParameter) {
        String salt = generateSalt(saltLength);
        return sha256WithSalt(password, salt + localParameter);
    }

    public static String sha256WithSaltAndParameter(String password, String localParameter) {
        String salt = generateSalt(5);
        return sha256WithSalt(password, salt + localParameter);
    }

    public static String sha256WithSaltAndParameter(String password, String salt, String localParameter) {
        return sha256WithSalt(password, salt + localParameter);
    }

    /**
     * SHA-512 hashing
     */

    public static String sha512(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            return byteArrayToHexString(byteData);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String sha512WithSalt(String password) {
        return sha512WithSalt(password, SALT_LENGTH_DEFAULT);
    }

    public static String sha512WithSalt(String password, String salt) {
        String passwordWithSalt = mergePassSalt(password, salt);
        return sha512(passwordWithSalt);
    }

    public static String sha512WithSalt(String password, int saltLength) {
        String salt = generateSalt(saltLength);
        return sha512WithSalt(password, salt);
    }

    public static String sha512WithSaltAndParameter(String password, int saltLength, String localParameter) {
        String salt = generateSalt(saltLength);
        return sha512WithSalt(password, salt + localParameter);
    }

    public static String sha512WithSaltAndParameter(String password, String localParameter) {
        String salt = generateSalt(5);
        return sha512WithSalt(password, salt + localParameter);
    }

    public static String sha512WithSaltAndParameter(String password, String salt, String localParameter) {
        return sha512WithSalt(password, salt + localParameter);
    }

    /**
     * Helpers
     */


    //TODO
    public static String generateSalt(int lenght) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < lenght; i++) {
            char c = chars[random.nextInt(chars.length)];
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    private static String mergePassSalt(String password, String salt) {
        return password + salt;
    }

    private static String byteArrayToHexString(byte[] byteData) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
