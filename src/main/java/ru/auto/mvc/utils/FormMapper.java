package ru.auto.mvc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.auto.mvc.form.*;
import ru.auto.mvc.form.viewForm.UserViewForm;
import ru.auto.mvc.model.*;
import ru.auto.mvc.model.enums.*;
import ru.auto.mvc.service.AutoService;
import ru.auto.mvc.service.UserProfileService;
import ru.auto.mvc.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@Component
public class FormMapper {
    @Autowired
    UserService userService;
    @Autowired
    UserProfileService userProfileService;
    @Autowired
    AutoService autoService;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

//    public CommentForm commentToForm(Comment comment) {
//        CommentForm commentForm = new CommentForm();
//        commentForm.setId(comment.getId());
//        commentForm.setUser(comment.getUser().getId());
//        commentForm.setReviewId(comment.getReview().getId());
//        commentForm.setCommentId(comment.getParrentComment().getId());
//        commentForm.setText(comment.getText());
//        commentForm.setPlus(comment.getPlus());
//        commentForm.setMinus(comment.getMinus());
//        commentForm.setDate(comment.getDate());
//        if (comment.getCommentList() == null) {
//            commentForm.setCommentList(null);
//        } else {
//            ArrayList<CommentForm> list = new ArrayList<CommentForm>();
//            for (Comment linkedComment : comment.getCommentList()) {
//                list.add(commentToCommentForm(linkedComment));
//            }
//            commentForm.setCommentList(list);
//        }
//        return commentForm;
//    }

    /**
     * User
     */

    public User userToModel(UserForm userForm) {
        User user;
        if (userForm.getId() != null) {
            user = new User(userForm.getId());
        } else {
            user = new User();
        }
        user.setLogin(userForm.getLogin());
        user.setPassword(userForm.getPassword());
        user.setEmail(userForm.getEmail());
        user.setExperience(userForm.getExperience());
        user.setKarma(userForm.getKarma());
        user.setRole(Role.getRole(userForm.getRole()));
        user.setProfile(userProfileService.findByid(userForm.getProfileId()));
        if (userForm.getAutoList() == null) {
            user.setAuto(null);
        } else {
            ArrayList<Auto> list = new ArrayList<Auto>();
            for (AutoForm auto : userForm.getAutoList()) {
                list.add(autoToModel(auto));
            }
            user.setAuto(list);
        }
        user.setSalt(userForm.getSalt());
        return user;
    }

    public UserForm userToForm(User user) {
        UserForm userForm = new UserForm();
        userForm.setId(user.getId());
        userForm.setLogin(user.getLogin());
        userForm.setPassword(user.getPassword());
        userForm.setEmail(user.getEmail());
        userForm.setExperience(user.getExperience());
        userForm.setKarma(user.getKarma());
        userForm.setRole(user.getRole().getValue());
        userForm.setProfileId(user.getProfile().getId());
        if (user.getAuto() == null) {
            userForm.setAutoList(null);
        } else {
            ArrayList<AutoForm> list = new ArrayList<AutoForm>();
            for (Auto auto : user.getAuto()) {
                list.add(autoToForm(auto));
            }
            userForm.setAutoList(list);
        }
        userForm.setSalt(user.getSalt());
        return userForm;
    }

    public UserViewForm userToViewForm(User user) {
        UserViewForm userViewForm = new UserViewForm();
        userViewForm.setId(user.getId());
        userViewForm.setLogin(user.getLogin());
        userViewForm.setExperience(user.getExperience());
        userViewForm.setKarma(user.getKarma());
        userViewForm.setRole(user.getRole().getValue());
        return userViewForm;
    }

    public UserViewForm userToViewForm(UserForm user) {
        UserViewForm userViewForm = new UserViewForm();
        userViewForm.setId(user.getId());
        userViewForm.setLogin(user.getLogin());
        userViewForm.setExperience(user.getExperience());
        userViewForm.setKarma(user.getKarma());
        userViewForm.setRole(user.getRole());
        return userViewForm;
    }

    public User userRegistrationToModel(UserRegistrationForm userRegForm) {
        User user = new User();
        user.setLogin(userRegForm.getLogin());
        user.setPassword(userRegForm.getPassword());
        user.setEmail(userRegForm.getEmail());
        user.setExperience(userRegForm.getExperience());
        if (userRegForm.getAutoList() == null) {
            user.setAuto(null);
        } else {
            ArrayList<Auto> list = new ArrayList<Auto>();
            for (AutoForm auto : userRegForm.getAutoList()) {
//                list.add(autoFormToAuto(auto));
            }
            user.setAuto(list);
        }
        return user;
    }

    public UserProfile userRegistrationToProfileModel(UserRegistrationForm userRegForm) throws ParseException {
        UserProfile userProfile = new UserProfile();
        userProfile.setName(userRegForm.getName());
        userProfile.setSurname(userRegForm.getSurname());
        userProfile.setGender(Gender.getGender(userRegForm.getGender()));
        userProfile.setLocation(userRegForm.getLocation());
        userProfile.setSummary(userRegForm.getSummary());
        if (userRegForm.getBirthDate() != null && !userRegForm.getBirthDate().equals("")) {
            userProfile.setBirthDate(sdf.parse(userRegForm.getBirthDate()));
        }
        return userProfile;
    }


    /**
     * UserProfile
     */

    public UserProfileForm userProfileToForm(UserProfile userProfile) {
        UserProfileForm userProfileForm = new UserProfileForm();
        userProfileForm.setId(userProfile.getId());
        userProfileForm.setName(userProfile.getName());
        userProfileForm.setSurname(userProfile.getSurname());
        userProfileForm.setGender(userProfile.getGender().getValue());
        userProfileForm.setEmailAccess(userProfile.getEmailAccess());
        userProfileForm.setLocation(userProfile.getLocation());
        userProfileForm.setSummary(userProfile.getSummary());
        if (userProfile.getBirthDate() != null) {
            userProfileForm.setBirthDate(sdf.format(userProfile.getBirthDate()));
        }
        if (userProfile.getRegDate() != null) {
            userProfileForm.setRegDate(sdf.format(userProfile.getRegDate()));
        }
        return userProfileForm;
    }

    public UserProfile userProfileToModel(UserProfileForm userProfileForm) throws ParseException {
        UserProfile userProfile = new UserProfile();
        userProfile.setId(userProfileForm.getId());
        userProfile.setName(userProfileForm.getName());
        userProfile.setSurname(userProfileForm.getSurname());
        userProfile.setGender(Gender.getGender(userProfileForm.getGender()));
        userProfile.setEmailAccess(userProfileForm.getEmailAccess());
        userProfile.setLocation(userProfileForm.getLocation());
        userProfile.setSummary(userProfileForm.getSummary());
        if (userProfileForm.getBirthDate() != null) {
            userProfile.setBirthDate(sdf.parse(userProfileForm.getBirthDate()));
        }
        if (userProfileForm.getRegDate() != null) {
            userProfile.setRegDate(sdf.parse(userProfileForm.getRegDate()));
        }
        return userProfile;
    }

    /**
     * Review
     */

    public ReviewForm reviewToForm(Review review) {
        ReviewForm reviewForm = new ReviewForm();
        reviewForm.setId(review.getId());
        reviewForm.setUser(userToViewForm(review.getUser()));
        reviewForm.setAutoId(review.getAuto().getId());
        reviewForm.setPros(review.getPros());
        reviewForm.setCons(review.getCons());
        reviewForm.setImpression(review.getImpression());
        reviewForm.setUpdateName(review.getUpdateName());
        reviewForm.setUpdateText(review.getUpdateText());
        if (review.getDate() != null) {
            reviewForm.setDate(sdf.format(review.getDate()));
        }
        reviewForm.setPlus(review.getPlus());
        reviewForm.setMinus(review.getMinus());
        reviewForm.setRate(review.getRate());
        reviewForm.setExpRate(review.getExpRate());
        reviewForm.setMotorText(review.getMotorText());
        reviewForm.setMotorRate(review.getMotorRate());
        reviewForm.setGearboxText(review.getGearboxText());
        reviewForm.setGearboxRate(review.getGearboxRate());
        reviewForm.setElectronicText(review.getElectronicText());
        reviewForm.setElectronicRate(review.getElectronicRate());
        reviewForm.setCarcassText(review.getCarcassText());
        reviewForm.setCarcassRate(review.getCarcassRate());
        reviewForm.setSuspentionText(review.getSuspentionText());
        reviewForm.setSuspentionRate(review.getSuspentionRate());
        reviewForm.setMileage(review.getMileage());
        reviewForm.setYear(review.getYear());
        reviewForm.setExperience(review.getExperience());
        return reviewForm;
    }

    public Review reviewToModel(ReviewForm reviewForm) throws ParseException {
        Review review = new Review(reviewForm.getId());
        if (reviewForm.getUser() != null) {
            review.setUser(userService.findByid(reviewForm.getUser().getId()));
        }
        review.setAuto(autoService.findByid(reviewForm.getAutoId()));
        review.setPros(reviewForm.getPros());
        review.setCons(reviewForm.getCons());
        review.setImpression(reviewForm.getImpression());
        review.setUpdateName(reviewForm.getUpdateName());
        review.setUpdateText(reviewForm.getUpdateText());
        if (reviewForm.getDate() != null) {
            review.setDate(sdf.parse(reviewForm.getDate()));
        }

        review.setPlus(reviewForm.getPlus());
        review.setMinus(reviewForm.getMinus());
        review.setRate(reviewForm.getRate());
        review.setExpRate(reviewForm.getExpRate());
        review.setMotorText(reviewForm.getMotorText());
        review.setMotorRate(reviewForm.getMotorRate());
        review.setGearboxText(reviewForm.getGearboxText());
        review.setGearboxRate(reviewForm.getGearboxRate());
        review.setElectronicText(reviewForm.getElectronicText());
        review.setElectronicRate(reviewForm.getElectronicRate());
        review.setCarcassText(reviewForm.getCarcassText());
        review.setCarcassRate(reviewForm.getCarcassRate());
        review.setSuspentionText(reviewForm.getSuspentionText());
        review.setSuspentionRate(reviewForm.getSuspentionRate());
        review.setMileage(reviewForm.getMileage());
        review.setYear(reviewForm.getYear());
        review.setExperience(reviewForm.getExperience());
        return review;
    }

    /**
     * Image
     */

    public Image imageToModel(ImageForm imageForm) {
        Image image = new Image(imageForm.getId());
        image.setUser(userService.findByid(imageForm.getUser().getId()));
        image.setLink(imageForm.getLink());
        return image;
    }

    public ImageForm imageToForm(Image image) {
        ImageForm imageForm = new ImageForm();
        imageForm.setId(image.getId());
        if (image.getUser() != null) {
            imageForm.setUser(userToViewForm(image.getUser()));
        }
        imageForm.setLink(image.getLink());
        return imageForm;
    }

    /**
     * Brand
     */

    public Brand brandToModel(BrandForm brandForm) {
        Brand brand = new Brand(brandForm.getId());
        brand.setBrand(brandForm.getBrand());
        return brand;
    }

    public BrandForm brandToForm(Brand brand) {
        BrandForm brandForm = new BrandForm();
        brandForm.setId(brand.getId());
        brandForm.setBrand(brand.getBrand());
        return brandForm;
    }

    /**
     * Engine
     */

    public Engine engineToModel(EngineForm engineForm) {
        Engine engine = new Engine(engineForm.getId());
        engine.setCapacity(engineForm.getCapacity());
        engine.setEngine(engineForm.getEngine());
        engine.setFuel(Fuel.getFuelByOriginal(engineForm.getFuel()));
        engine.setTypeOfEngine(TypeOfEngine.getTypeOfEngineByOriginal(engineForm.getTypeOfEngine()));
        return engine;
    }

    public EngineForm engineToForm(Engine engine) {
        EngineForm engineForm = new EngineForm();
        engineForm.setCapacity(engine.getCapacity());
        engineForm.setEngine(engine.getEngine());
        engineForm.setFuel(engine.getFuel().getValue());
        engineForm.setTypeOfEngine(engine.getTypeOfEngine().getValue());
        return engineForm;
    }

    /**
     * Generation
     */

    public Generation generationToModel(GenerationForm generationForm) {
        Generation generation = new Generation(generationForm.getId());
        if (generationForm.getBeginYear() != null) {
            generation.setBeginYear(generationForm.getBeginYear());
        }
        if (generationForm.getEndYear() != null) {
            generation.setEndYear(generationForm.getEndYear());
        }
        if (generationForm.getGeneration() != null) {
            generation.setGeneration(generationForm.getGeneration());
        }
        if (generationForm.getModel() != null) {
            generation.setModel(modelToModel(generationForm.getModel()));
        }
        return generation;
    }

    public GenerationForm generationToForm(Generation generation) {
        GenerationForm generationForm = new GenerationForm();
        generationForm.setGeneration(generation.getGeneration());
        generationForm.setBeginYear(generation.getBeginYear());
        generationForm.setEndYear(generation.getEndYear());
        generationForm.setModel(modelToForm(generation.getModel()));
        return generationForm;
    }

    /**
     * Model
     */

    public Model modelToModel(ModelForm modelForm) {
        Model model = new Model(modelForm.getIdModel());
        if (modelForm.getBrand() != null) {
            model.setBrand(brandToModel(modelForm.getBrand()));
        }
        model.setModel(modelForm.getModel());
        return model;
    }

    public ModelForm modelToForm(Model model) {
        ModelForm modelForm = new ModelForm();
        modelForm.setBrand(brandToForm(model.getBrand()));
        modelForm.setModel(model.getModel());
        return modelForm;
    }

    /**
     * Auto
     */

    public Auto autoToModel(AutoForm autoForm) {
        Auto auto = new Auto(autoForm.getId());
        auto.setBrand(brandToModel(autoForm.getBrand()));
        auto.setBody(Body.getBodyByOriginal(autoForm.getBody()));
        auto.setDrivetrain(Drivetrain.getDrivetrainByOriginal(autoForm.getDrivetrain()));
        if (autoForm.getEngine() != null) {
            auto.setEngine(engineToModel(autoForm.getEngine()));
        }
        if (autoForm.getGeneration() != null) {
            auto.setGeneration(generationToModel(autoForm.getGeneration()));
        }
        if (autoForm.getModel() != null) {
            auto.setModel(modelToModel(autoForm.getModel()));
        }
        auto.setTransmission(Transmission.getTransmissionByOriginal(autoForm.getTransmission()));
        if (autoForm.getImages() == null) {
            auto.setImages(null);
        } else {
            ArrayList<Image> list = new ArrayList<Image>();
            for (ImageForm image : autoForm.getImages()) {
                list.add(imageToModel(image));
            }
            auto.setImages(list);
        }
        auto.setEuroNCAP(autoForm.getEuroNCAP());
        return auto;
    }

    public AutoForm autoToForm(Auto auto) {
        AutoForm autoForm = new AutoForm();
        autoForm.setId(auto.getId());
        autoForm.setBrand(brandToForm((auto.getBrand())));
        autoForm.setBody(auto.getBody().getValue());
        autoForm.setDrivetrain(auto.getDrivetrain().getValue());
        autoForm.setEngine(engineToForm(auto.getEngine()));
        autoForm.setGeneration(generationToForm(auto.getGeneration()));
        autoForm.setModel(modelToForm(auto.getModel()));
        autoForm.setTransmission(auto.getTransmission().getValue());
        if (auto.getImages() == null) {
            autoForm.setImages(null);
        } else {
            ArrayList<ImageForm> list = new ArrayList<ImageForm>();
            for (Image image : auto.getImages()) {
                list.add(imageToForm(image));
            }
            autoForm.setImages(list);
        }
        autoForm.setEuroNCAP(auto.getEuroNCAP());
        return autoForm;
    }
}