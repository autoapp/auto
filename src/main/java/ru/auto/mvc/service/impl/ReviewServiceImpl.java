package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.ReviewDao;
import ru.auto.mvc.model.Review;
import ru.auto.mvc.service.ReviewService;

import java.util.List;

@Service("ReviewService")
@Transactional
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewDao reviewDao;


    @Override
    public Long add(Review review) {
        return reviewDao.save(review);
    }

    @Override
    public Review findById(Long id) {
        return reviewDao.findById(id);
    }

    @Override
    public List<Review> searchReaviews(Long id) {
        return reviewDao.findAllById(id);
    }


}
