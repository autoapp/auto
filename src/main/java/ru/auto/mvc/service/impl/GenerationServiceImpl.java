package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.GenerationDao;
import ru.auto.mvc.model.Generation;
import ru.auto.mvc.service.GenerationService;

import java.util.List;

/**
 * Created by EduardL on 01.05.15.
 */

@Service
public class GenerationServiceImpl implements GenerationService {
    @Autowired
    GenerationDao generationDao;

    @Override
    @Transactional
    public List<Generation> allGenerations() {
        return generationDao.findAll();
    }
}
