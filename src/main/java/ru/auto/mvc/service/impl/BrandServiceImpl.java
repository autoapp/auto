package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.BrandDao;
import ru.auto.mvc.model.Brand;
import ru.auto.mvc.service.BrandService;
import sun.management.resources.agent_pt_BR;

import java.util.List;

/**
 * Created by EduardL on 30.04.15.
 */
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandDao brandDao;

    @Override
    @Transactional
    public List<Brand> allBrands() {
        return brandDao.findAll();
    }
}
