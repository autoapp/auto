package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.ModelDao;
import ru.auto.mvc.model.Model;
import ru.auto.mvc.service.ModelService;

import java.util.List;

/**
 * Created by EduardL on 30.04.15.
 */
@Service
public class ModelServiceImpl implements ModelService{

    @Autowired
    ModelDao modelDao;

    @Override
    @Transactional
    public List<Model> allModels() {
        return modelDao.findAll();
    }
}
