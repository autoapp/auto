package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.AutoDao;
import ru.auto.mvc.form.AutoForm;
import ru.auto.mvc.model.Auto;
import ru.auto.mvc.service.AutoService;

import java.util.List;

@Service("AutoService")
@Transactional
public class AutoServiceImpl implements AutoService {

    @Autowired
    AutoDao autoDao;

    @Override
    @Transactional
    public Auto findByid(Long id) {
        return autoDao.findById(id);
    }

    @Override
    @Transactional
    public List<Auto> search(Auto auto, Double beginCapacity, Double endCapacity) {
         return autoDao.search(auto, beginCapacity, endCapacity);
    }

}
