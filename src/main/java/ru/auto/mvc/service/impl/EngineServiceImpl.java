package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.EngineDao;
import ru.auto.mvc.model.Engine;
import ru.auto.mvc.service.EngineService;

import java.util.List;

/**
 * Created by EduardL on 02.05.15.
 */
@Service
public class EngineServiceImpl implements EngineService {
    @Autowired
    EngineDao engineDao;

    @Override
    @Transactional
    public List<Engine> allEngines() {
        return engineDao.findAll();
    }
}
