package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.UserDao;
import ru.auto.mvc.model.User;
import ru.auto.mvc.service.UserService;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    public User findByid(Long id){
        return userDao.findById(id);
    }

    @Override
    public Long addUser(User user) {
        return userDao.save(user);
    }

    @Override
    public void edit(User user) {
        userDao.edit(user);
    }

}
