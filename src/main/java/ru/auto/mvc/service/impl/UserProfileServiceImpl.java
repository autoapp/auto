package ru.auto.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.auto.mvc.dao.UserProfileDao;
import ru.auto.mvc.model.UserProfile;
import ru.auto.mvc.service.UserProfileService;

@Service("UserProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    UserProfileDao userProfileDao;


    @Override
    public UserProfile findByid(Long id){
        return userProfileDao.findById(id);
    }

    @Override
    public Long save(UserProfile userProfile) {
        return userProfileDao.save(userProfile);
    }

}
