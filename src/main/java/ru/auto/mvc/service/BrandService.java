package ru.auto.mvc.service;

import ru.auto.mvc.model.Brand;

import java.util.List;

/**
 * Created by EduardL on 30.04.15.
 */
public interface BrandService {
    List<Brand> allBrands();
}
