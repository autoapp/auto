package ru.auto.mvc.service;

import ru.auto.mvc.model.Engine;

import java.util.List;

/**
 * Created by EduardL on 02.05.15.
 */
public interface EngineService {
    List<Engine> allEngines();
}
