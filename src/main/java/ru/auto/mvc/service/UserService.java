package ru.auto.mvc.service;

import ru.auto.mvc.model.User;

public interface UserService {

    User findByName(String name);

    User findByid(Long id);

    Long addUser(User user);

    void edit(User user);
}
