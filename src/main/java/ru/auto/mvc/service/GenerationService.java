package ru.auto.mvc.service;

import ru.auto.mvc.model.Generation;

import java.util.List;

/**
 * Created by EduardL on 01.05.15.
 */
public interface GenerationService {
    List<Generation> allGenerations();
}
