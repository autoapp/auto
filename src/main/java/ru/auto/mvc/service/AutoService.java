package ru.auto.mvc.service;

import ru.auto.mvc.form.AutoForm;
import ru.auto.mvc.model.Auto;

import java.util.List;

public interface AutoService {

    Auto findByid(Long id);

    List<Auto> search(Auto auto, Double beginCapacity, Double endCapacity);
}
