package ru.auto.mvc.service;

import ru.auto.mvc.model.Review;

import java.util.List;

public interface ReviewService {

    Long add(Review review);

    Review findById(Long id);

    List<Review> searchReaviews(Long id);
}
