package ru.auto.mvc.service;

import ru.auto.mvc.model.UserProfile;

public interface UserProfileService {

    UserProfile findByid(Long id);

    Long save(UserProfile userProfile);
}
