package ru.auto.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.auto.mvc.model.User;
import ru.auto.mvc.utils.AuthenticationUtil;

@Controller
public class HomePageController {


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(ModelMap map) {
        User user = AuthenticationUtil.getAuthUser();
        if (user == null) {
            return "redirect:/login";
        }
//        return "redirect:/page/" + user.getName();
        return "redirect:/searchPanel";
    }
}
