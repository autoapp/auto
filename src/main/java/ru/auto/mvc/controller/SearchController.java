package ru.auto.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.auto.mvc.form.*;
import ru.auto.mvc.model.*;
import ru.auto.mvc.model.enums.*;
import ru.auto.mvc.service.*;
import ru.auto.mvc.utils.FormMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EduardL on 28.04.15.
 */


@Controller
public class SearchController {

    @Autowired
    AutoService autoService;
    @Autowired
    FormMapper formMapper;
    @Autowired
    BrandService brandService;
    @Autowired
    ModelService modelService;
    @Autowired
    GenerationService generationService;
    @Autowired
    EngineService engineService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@ModelAttribute("auto") AutoForm autoForm, @RequestParam("idBrand") Long idBrand,  @RequestParam("idModel")Long idModel, @RequestParam("idGeneration") Long idGeneration,
                         @RequestParam("beginYear") int beginYear, @RequestParam("endYear") int endYear, @RequestParam("typeOfEngine") String typeOfEngine,
                         @RequestParam("idEngine") Long idEngine,@RequestParam("beginCapacity") Double beginCapacity, @RequestParam("endCapacity") Double endCapacity,
                         @RequestParam("fuel") String fuel, ModelMap map) {

        BrandForm brandForm = new BrandForm();
        brandForm.setId(idBrand);

        ModelForm modelForm = new ModelForm();
        modelForm.setIdModel(idModel);

        GenerationForm generationForm = new GenerationForm();
        generationForm.setId(idGeneration);
        generationForm.setBeginYear(beginYear);
        generationForm.setEndYear(endYear);

        EngineForm engineForm = new EngineForm();
        engineForm.setId(idEngine);
        engineForm.setFuel(fuel);
        engineForm.setTypeOfEngine(typeOfEngine);

        autoForm.setBrand(brandForm);
        autoForm.setModel(modelForm);
        autoForm.setGeneration(generationForm);
        autoForm.setEngine(engineForm);

        Auto auto = formMapper.autoToModel(autoForm);

        List<Auto> autos = autoService.search(auto, beginCapacity, endCapacity);
        List<AutoForm> autoForms = new ArrayList<AutoForm>();

        if (autos != null && !autos.isEmpty()) {
            for (int i = 0; i < autos.size(); i++) {
                autoForms.add(formMapper.autoToForm(autos.get(i)));
            }
        }

        System.out.println("I working");
        if (autos != null) {
            if (autos.isEmpty()) {
                System.out.println("I'm empty :(");
            }
            for (int i = 0; i < autos.size(); i++) {
                System.out.println("Brand from for " + autos.get(i).getBrand().getBrand());
                System.out.println("Auto " + autos.get(i).getId());
            }
        }
        map.addAttribute("searchedCars", autoForms);
        return "searchedCars";
    }

    @RequestMapping(value = "/searchPanel", method = RequestMethod.GET)
    public String page(ModelMap map) {
        List<Brand> brands = brandService.allBrands();
        List<Model> models = modelService.allModels();
        List<Generation> generations = generationService.allGenerations();
        List<Engine> engines = engineService.allEngines();
        map.addAttribute("allBrands", brands);
        map.addAttribute("auto", new AutoForm());
        map.addAttribute("allModels", models);
        map.addAttribute("allGenerations", generations);
        map.addAttribute("allTypeOfEngines", TypeOfEngine.values());
        map.addAttribute("allEngines", engines);
        map.addAttribute("allFuels", Fuel.values());
        map.addAttribute("allDrivetrains", Drivetrain.values());
        map.addAttribute("allTransmissions", Transmission.values());
        map.addAttribute("allBodys", Body.values());
        return "mainPage";
    }

    @RequestMapping(value = "/plusReview", method = RequestMethod.POST)
    public void plusRev(@RequestParam("") Integer rating) {

    }
}
