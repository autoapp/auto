package ru.auto.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.auto.mvc.model.Review;
import ru.auto.mvc.service.AutoService;
import ru.auto.mvc.service.ReviewService;

/**
 * Created by EduardL on 13.05.15.
 */
@Controller
public class CurrentCar {
    @Autowired
    AutoService autoService;
    @Autowired
    ReviewService reviewService;

    @RequestMapping(value = {"/auto/{idCar}"}, method = RequestMethod.GET)
    public String currentCar(@PathVariable("idCar") Long idCar, ModelMap map) {
        map.addAttribute("currentCar", autoService.findByid(idCar)).addAttribute("listReviews", reviewService.searchReaviews(idCar));
        return "currentCar";
    }

}
