package ru.auto.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.auto.mvc.model.User;
import ru.auto.mvc.utils.AuthenticationUtil;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error, ModelMap map) {
        if (error != null) {
            map.addAttribute("error", "Invalid username or password!");
        }
        User user = AuthenticationUtil.getAuthUser();
        if (user != null)
            return "redirect:/";
        else
            return "login";
    }
}
