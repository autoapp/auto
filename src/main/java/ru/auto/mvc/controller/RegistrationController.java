package ru.auto.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.auto.mvc.form.UserRegistrationForm;
import ru.auto.mvc.model.User;
import ru.auto.mvc.model.UserProfile;
import ru.auto.mvc.model.enums.Role;
import ru.auto.mvc.service.UserProfileService;
import ru.auto.mvc.service.UserService;
import ru.auto.mvc.utils.FormMapper;
import ru.auto.mvc.utils.PasswordCoder;

import java.text.ParseException;
import java.util.Date;

@Controller
public class RegistrationController {

    @Autowired
    @Qualifier("UserService")
    UserService userService;
    @Autowired
    @Qualifier("UserProfileService")
    UserProfileService userProfileService;
    @Autowired
    FormMapper formMapper;

    //TODO validator

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String renderRegPage(ModelMap map) {
        map.addAttribute("UserRegistrationForm", new UserRegistrationForm());
        return "registration";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("UserRegistrationForm") UserRegistrationForm userRegForm, ModelMap map) throws ParseException {
        if (userService.findByName(userRegForm.getLogin()) != null
                || !userRegForm.getPassword().equals(userRegForm.getConfirmPassword())) {
            return "registration";
        } else {
            User user = formMapper.userRegistrationToModel(userRegForm);
            UserProfile userProfile = formMapper.userRegistrationToProfileModel(userRegForm);
            user.setKarma(0.0);
            user.setRole(Role.AUTHORIZED);
            user.setSalt(PasswordCoder.generateSalt(4));
            user.setPassword(PasswordCoder
                    .sha512WithSaltAndParameter(user.getPassword(), user.getSalt(), "addition"));
            userProfile.setEmailAccess(false);
            userProfile.setRegDate(new Date());
            userProfile.setId(userProfileService.save(userProfile));
            user.setProfile(userProfile);
            userService.addUser(user);
            return "redirect:/";

        }
    }

    @RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
    public Boolean checkLogin(@RequestParam("login") String login, ModelMap map) {

        return true;
    }

    @RequestMapping(value = "/checkEmail", method = RequestMethod.POST)
    public Boolean checkEmail(@RequestParam("login") String login, ModelMap map) {

        return true;
    }

}
