package ru.auto.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.auto.mvc.form.*;
import ru.auto.mvc.form.viewForm.UserViewForm;
import ru.auto.mvc.model.*;
import ru.auto.mvc.model.enums.*;
import ru.auto.mvc.service.*;
import ru.auto.mvc.utils.AuthenticationUtil;
import ru.auto.mvc.utils.FormMapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rigen on 05.05.15.
 */

@Controller
public class ProfileController {

    @Autowired
    AutoService autoService;
    @Autowired
    UserService userService;
    @Autowired
    FormMapper formMapper;
    @Autowired
    BrandService brandService;
    @Autowired
    ModelService modelService;
    @Autowired
    GenerationService generationService;
    @Autowired
    EngineService engineService;
    @Autowired
    ReviewService reviewService;

    @RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
    public String authUserProfile(ModelMap map) {
        User user = AuthenticationUtil.getAuthUser();
        if (user != null) {
            return "redirect:/profile/" + user.getId();
        }
        return "redirect:/";
    }

    @RequestMapping(value = {"/profile/{id}"}, method = RequestMethod.GET)
    public String userProfile(@PathVariable("id") Long id, ModelMap map) {
        User user = userService.findByid(id);
        UserViewForm userViewForm;
        UserProfileForm userProfileForm;
        if (user != null) {
            userViewForm = formMapper.userToViewForm(user);
            userProfileForm = formMapper.userProfileToForm(user.getProfile());
        } else {
            return "redirect:/";
        }
        map.addAttribute("user", user);
        map.addAttribute("userProfile", userProfileForm);
        User authUser = AuthenticationUtil.getAuthUser();
        UserViewForm authUserViewForm = null;
        List<AutoForm> autoFormList = new ArrayList<AutoForm>();
        if (authUser != null) {
            authUserViewForm = formMapper.userToViewForm(authUser);
        }
        if (user.getAuto() != null) {
            List<Auto> autoList = user.getAuto();
            for (Auto auto : autoList) {
                autoFormList.add(formMapper.autoToForm(auto));
            }
        }
        map.addAttribute("autoList", autoFormList);
        HashMap reviewMap = new HashMap();
        for (AutoForm autoForm : autoFormList) {

            List<Review> reviewList = reviewService.searchReaviews(autoForm.getId());
            List<ReviewForm> reviewForms = new ArrayList<ReviewForm>();
            for (Review review : reviewList) {
                reviewForms.add(formMapper.reviewToForm(review));
            }

            reviewMap.put(autoForm.getId(), reviewForms);

        }

        map.addAttribute("reviewMap", reviewMap);
        map.addAttribute("authUser", authUserViewForm);

        List<Brand> brands = brandService.allBrands();
        List<Model> models = modelService.allModels();
        List<Generation> generations = generationService.allGenerations();
        List<Engine> engines = engineService.allEngines();
        map.addAttribute("allBrands", brands);
        map.addAttribute("allModels", models);
        map.addAttribute("allGenerations", generations);
        map.addAttribute("allTypeOfEngines", TypeOfEngine.values());
        map.addAttribute("allEngines", engines);
        map.addAttribute("allFuels", Fuel.values());
        map.addAttribute("allDrivetrains", Drivetrain.values());
        map.addAttribute("allTransmissions", Transmission.values());
        map.addAttribute("allBodys", Body.values());
        map.addAttribute("submit", false);

        return "profile";
    }

    @ResponseBody
    @RequestMapping(value = {"/addAuto"}, method = RequestMethod.POST)
    public AutoForm addAuto(@ModelAttribute("auto") AutoForm autoForm, @RequestParam("idBrand") Long idBrand, @RequestParam("idModel") Long idModel, @RequestParam("idGeneration") Long idGeneration,
                            @RequestParam("beginYear") int beginYear, @RequestParam("endYear") int endYear, @RequestParam("typeOfEngine") String typeOfEngine,
                            @RequestParam("idEngine") Long idEngine, @RequestParam("beginCapacity") Double beginCapacity, @RequestParam("endCapacity") Double endCapacity,
                            @RequestParam("fuel") String fuel, ModelMap map) {

        BrandForm brandForm = new BrandForm();
        brandForm.setId(idBrand);

        ModelForm modelForm = new ModelForm();
        modelForm.setIdModel(idModel);

        GenerationForm generationForm = new GenerationForm();
        generationForm.setId(idGeneration);
        generationForm.setBeginYear(beginYear);
        generationForm.setEndYear(endYear);

        EngineForm engineForm = new EngineForm();
        engineForm.setId(idEngine);
        engineForm.setFuel(fuel);
        engineForm.setTypeOfEngine(typeOfEngine);

        autoForm.setBrand(brandForm);
        autoForm.setModel(modelForm);
        autoForm.setGeneration(generationForm);
        autoForm.setEngine(engineForm);

        Auto auto = formMapper.autoToModel(autoForm);

        List<Auto> autos = autoService.search(auto, beginCapacity, endCapacity);
        if (autos == null || autos.isEmpty() || autos.size() > 1) {
            return null;
        } else {
            User user = AuthenticationUtil.getAuthUser();
            if (user.getAuto() != null) {
                user.getAuto().add(autos.get(0));
            } else {
                user.setAuto(autos);
            }
            userService.edit(user);
            AutoForm autoForms = formMapper.autoToForm(autos.get(0));
            return autoForms;
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/addReview"}, method = RequestMethod.POST)
    public ReviewForm addReview(@ModelAttribute("review") ReviewForm reviewForm) throws ParseException {
        Review review = formMapper.reviewToModel(reviewForm);
        review.setUser(AuthenticationUtil.getAuthUser());
        review.setDate(new Date());
        reviewService.add(review);
        return reviewForm;
    }

//    @ResponseBody
//    @RequestMapping(value="/uploadImage", method=RequestMethod.POST)
//    public String handleFileUpload(@RequestParam("name") String name,
//                                                 @RequestParam("file") MultipartFile file){
//        if (!file.isEmpty()) {
//            try {
//                byte[] bytes = file.getBytes();
//                BufferedOutputStream stream =
//                        new BufferedOutputStream(new FileOutputStream(new File(name)));
//                stream.write(bytes);
//                stream.close();
//                return "You successfully uploaded " + name + "!";
//            } catch (Exception e) {
//                return "You failed to upload " + name + " => " + e.getMessage();
//            }
//        } else {
//            return "You failed to upload " + name + " because the file was empty.";
//        }
//    }
}
