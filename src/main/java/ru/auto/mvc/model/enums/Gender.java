package ru.auto.mvc.model.enums;

/**
 * Created by Rigen on 22.04.15.
 */
public enum Gender {
    MALE("мужской"),
    FEMALE("женский");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    public static Gender getGender(String value) {
        for (Gender gender : Gender.values()) {
            if (gender.getValue().equals(value)) {
                return gender;
            }
        }
        return Gender.MALE;
    }

    public String getValue() {
        return value;
    }
}
