package ru.auto.mvc.model.enums;

/**
 * Created by Rigen on 22.04.15.
 */
public enum Role {
    AUTHORIZED("Пользователь"),
    UNAUTHORIZED("Гость"),
    MECHANIC("Механик"),
    ADMIN("Администратор");


    private String value;


    Role(String value) {
        this.value = value;
    }

    public static Role getRole(String value) {
        for (Role role : Role.values()) {
            if(role.getValue().equals(value)) {
                return role;
            }
        }
        return Role.AUTHORIZED;
    }

    public String getValue() {
        return value;
    }
}
