package ru.auto.mvc.model.enums;

/**
 * Created by EduardL on 24.04.15.
 */
public enum Body {
    SEDAN("Седан"),
    SUV("Внедорожник"),
    WAGON("Универсал"),
    HATCHBACK("Хэтчбек"),
    VAN("Минивэн");

    private String value;

    Body(String value) {
        this.value = value;
    }

    public static Body getBody(String value) {
        for (Body body : Body.values()) {
            if (body.getValue().equals(value)) {
                return body;
            }
        }
        return null;
    }

    public static Body getBodyByOriginal(String value) {
        for (Body body : Body.values()) {
            if (body.toString().equals(value)) {
                return body;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
