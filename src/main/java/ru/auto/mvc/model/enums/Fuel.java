package ru.auto.mvc.model.enums;

/**
 * Created by EduardL on 22.04.15.
 */
public enum Fuel {
    GASOLINE("Бензин"),
    GASOLINE_HYBRID("Гибрид"),
    DIESEL("Дизель"),
    ELECTRIC("Электро");

    private String value;
    Fuel(String value) {
        this.value = value;
    }

    public static Fuel getFuel(String value) {
        for (Fuel fuel : Fuel.values()) {
            if (fuel.getValue().equals(value)) {
                return fuel;
            }
        }
        return null;
    }

    public static Fuel getFuelByOriginal(String value) {
        for (Fuel fuel : Fuel.values()) {
            if (fuel.toString().equals(value)) {
                return fuel;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
