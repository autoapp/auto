package ru.auto.mvc.model.enums;

/**
 * Created by EduardL on 22.04.15.
 */

/**
 * Привод: полный, передний, задний
 */
public enum Drivetrain {
    D4X4("Полный"),
    HEAD("Передний"),
    BACK("Задний");

    private String value;

    Drivetrain(String value) {
        this.value = value;
    }

    public static Drivetrain getDrivetrain(String value) {
        for (Drivetrain drivetrain : Drivetrain.values()) {
            if (drivetrain.getValue().equals(value)) {
                return drivetrain;
            }
        }
        return null;
    }

    public static Drivetrain getDrivetrainByOriginal(String value) {
        for (Drivetrain drivetrain : Drivetrain.values()) {
            if (drivetrain.toString().equals(value)) {
                return drivetrain;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
