package ru.auto.mvc.model.enums;

/**
 * Created by EduardL on 23.04.15.
 */
public enum TypeOfEngine {
    ATMOSPHERIC_ENGINE("Атмосферный"),
    BOOST_ENGINE("С наддувом");

    private String value;


    TypeOfEngine(String value) {
        this.value = value;
    }

    public static TypeOfEngine getTypeOfEngine(String value) {
        for (TypeOfEngine typeOfEngine : TypeOfEngine.values()) {
            if(typeOfEngine.getValue().equals(value)) {
                return typeOfEngine;
            }
        }
        return null;
    }

    public static TypeOfEngine getTypeOfEngineByOriginal(String value) {
        for (TypeOfEngine typeOfEngine : TypeOfEngine.values()) {
            if(typeOfEngine.toString().equals(value)) {
                return typeOfEngine;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
