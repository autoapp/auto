package ru.auto.mvc.model.enums;

/**
 * Created by EduardL on 24.04.15.
 */
public enum Transmission {
    MANUAL("Механическая"),
    AUTOMATIC("Автоматическая"),
    CVT("Вариатор"),
    ROBOT("Роботизированная");


    private String value;

    Transmission(String value) {
        this.value = value;
    }

    public static Transmission getTransmission(String value) {
        for (Transmission transmission : Transmission.values()) {
            if (transmission.getValue().equals(value)) {
                return transmission;
            }
        }
        return null;
    }

    public static Transmission getTransmissionByOriginal(String value) {
        for (Transmission transmission : Transmission.values()) {
            if (transmission.toString().equals(value)) {
                return transmission;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
