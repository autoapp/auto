package ru.auto.mvc.model;

import javax.persistence.*;

/**
 * Created by Rigen on 22.04.15.
 */
@Entity(name = "image")
@Table(name = "image")
public class Image {
    @Id
    @SequenceGenerator(name = "ImageIdSequence", sequenceName = "Image_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ImageIdSequence")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column
    private String link;


    public Image() {
    }

    public Image(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
