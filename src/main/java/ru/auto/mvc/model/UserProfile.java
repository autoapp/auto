package ru.auto.mvc.model;

import org.hibernate.annotations.Type;
import ru.auto.mvc.model.enums.Gender;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Rigen on 22.04.15.
 */
@Entity(name = "user_profile")
@Table(name = "user_profile")
public class UserProfile {
    @Id
    @SequenceGenerator(name = "UserProfileIdSequence", sequenceName = "user_profile_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserProfileIdSequence")
    private Long id;
    private String name;
    private String surname;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private Boolean emailAccess;
    private String location;
    private String summary;
    @Type(type="timestamp")
    private Date birthDate;
    @Type(type="timestamp")
    private Date regDate;

    public UserProfile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getEmailAccess() {
        return emailAccess;
    }

    public void setEmailAccess(Boolean emailAccess) {
        this.emailAccess = emailAccess;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
}
