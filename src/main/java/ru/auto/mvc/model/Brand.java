package ru.auto.mvc.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
@Entity(name = "brand")
@Table(name = "brand")
public class Brand {
    @Id
    @SequenceGenerator(name = "BrandIdSequence", sequenceName = "Brand_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BrandIdSequence")
    private Long id;

    @Column(name="brand")
    private String brand;

    public Brand() {
    }

    public Brand(Long id) {
        this.id = id;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
