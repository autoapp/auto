package ru.auto.mvc.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Rigen on 23.04.15.
 */
@Entity(name = "review")
@Table(name = "review")
public class Review {
    @Id
    @SequenceGenerator(name = "ReviewIdSequence", sequenceName = "Review_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReviewIdSequence")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "auto_id")
    private Auto auto;
    private String pros;
    private String cons;
    private String impression;
    @Column(name = "update_name")
    private String updateName;
    @Column(name = "update_text")
    private String updateText;
    @Type(type = "timestamp")
    private Date date;
    private Integer plus;
    private Integer minus;
    private Double rate; //TODO
    @Column(name = "exp_rate")
    private Integer expRate; //TODO
    @Column(name = "motor_text")
    private String motorText;
    @Column(name = "motor_rate")
    private Integer motorRate;
    @Column(name = "gearbox_text")
    private String gearboxText;
    @Column(name = "gearbox_rate")
    private Integer gearboxRate;
    @Column(name = "electronic_text")
    private String electronicText;
    @Column(name = "electronic_rate")
    private Integer electronicRate;
    @Column(name = "carcass_text")
    private String carcassText;
    @Column(name = "carcass_rate")
    private Integer carcassRate;
    @Column(name = "suspention_text")
    private String suspentionText;
    @Column(name = "suspention_rate")
    private Integer suspentionRate;
    private Integer mileage;
    private Integer year;
    private Integer experience;

    public Review() {
    }

    public Review(Long id) {
        this.id = id;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public String getPros() {
        return pros;
    }

    public void setPros(String pros) {
        this.pros = pros;
    }

    public String getCons() {
        return cons;
    }

    public void setCons(String cons) {
        this.cons = cons;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateText() {
        return updateText;
    }

    public void setUpdateText(String updateText) {
        this.updateText = updateText;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPlus() {
        return plus;
    }

    public void setPlus(Integer plus) {
        this.plus = plus;
    }

    public Integer getMinus() {
        return minus;
    }

    public void setMinus(Integer minus) {
        this.minus = minus;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getExpRate() {
        return expRate;
    }

    public void setExpRate(Integer expRate) {
        this.expRate = expRate;
    }

    public String getMotorText() {
        return motorText;
    }

    public void setMotorText(String motorText) {
        this.motorText = motorText;
    }

    public Integer getMotorRate() {
        return motorRate;
    }

    public void setMotorRate(Integer motorRate) {
        this.motorRate = motorRate;
    }

    public String getGearboxText() {
        return gearboxText;
    }

    public void setGearboxText(String gearboxText) {
        this.gearboxText = gearboxText;
    }

    public Integer getGearboxRate() {
        return gearboxRate;
    }

    public void setGearboxRate(Integer gearboxRate) {
        this.gearboxRate = gearboxRate;
    }

    public String getElectronicText() {
        return electronicText;
    }

    public void setElectronicText(String electronicText) {
        this.electronicText = electronicText;
    }

    public Integer getElectronicRate() {
        return electronicRate;
    }

    public void setElectronicRate(Integer electronicRate) {
        this.electronicRate = electronicRate;
    }

    public String getCarcassText() {
        return carcassText;
    }

    public void setCarcassText(String carcassText) {
        this.carcassText = carcassText;
    }

    public Integer getCarcassRate() {
        return carcassRate;
    }

    public void setCarcassRate(Integer carcassRate) {
        this.carcassRate = carcassRate;
    }

    public String getSuspentionText() {
        return suspentionText;
    }

    public void setSuspentionText(String suspentionText) {
        this.suspentionText = suspentionText;
    }

    public Integer getSuspentionRate() {
        return suspentionRate;
    }

    public void setSuspentionRate(Integer suspentionRate) {
        this.suspentionRate = suspentionRate;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }
}
