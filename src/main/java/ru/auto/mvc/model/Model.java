package ru.auto.mvc.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
@Entity(name = "model")
@Table(name = "model")
public class Model {
    @Id
    @SequenceGenerator(name = "ModelIdSequence", sequenceName = "Model_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ModelIdSequence")
    private Long id;

    @Column(name = "model")
    private String model;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    public Model() {
    }

    public Model(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}
