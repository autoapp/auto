package ru.auto.mvc.model;

import ru.auto.mvc.model.enums.Body;
import ru.auto.mvc.model.enums.Drivetrain;
import ru.auto.mvc.model.enums.Transmission;

import javax.persistence.*;
import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
@Entity(name = "auto")
@Table(name = "auto")
public class Auto {
    @Id
    @SequenceGenerator(name = "AutoIdSequence", sequenceName = "Auto_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AutoIdSequence")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    private Integer euroNCAP;

    @Enumerated(EnumType.STRING)
    private Drivetrain drivetrain;

    @ManyToOne
    @JoinColumn(name = "generation_id")
    private Generation generation;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(uniqueConstraints= {@UniqueConstraint(columnNames={"auto_id","images_id"})}, name = "autosAndImages",
            joinColumns =  {@JoinColumn(name = "auto_id")}, inverseJoinColumns = @JoinColumn(name = "images_id"))
    private List<Image> images;


    @Enumerated(EnumType.STRING)
    private Transmission transmission;

    @ManyToOne
    @JoinColumn(name = "engine_id")
    private Engine engine;

    @Column
    @Enumerated(EnumType.STRING)
    private Body body;

    public Auto() {
    }

    public Auto(Long id) {
        this.id=id;
    }

    public Integer getEuroNCAP() {
        return euroNCAP;
    }

    public void setEuroNCAP(Integer euroNCAP) {
        this.euroNCAP = euroNCAP;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Drivetrain getDrivetrain() {
        return drivetrain;
    }

    public void setDrivetrain(Drivetrain drivetrain) {
        this.drivetrain = drivetrain;
    }

    public Generation getGeneration() {
        return generation;
    }

    public void setGeneration(Generation generation) {
        this.generation = generation;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
