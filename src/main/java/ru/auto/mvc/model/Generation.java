package ru.auto.mvc.model;

import ru.auto.mvc.model.enums.Drivetrain;
import ru.auto.mvc.model.enums.Transmission;

import javax.persistence.*;
import java.util.List;

/**
 * Created by EduardL on 24.04.15.
 */
@Entity(name = "generation")
@Table(name = "generation")
public class Generation {
    @Id
    @SequenceGenerator(name = "GenerationIdSequence", sequenceName = "Generation_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GenerationIdSequence")
    private Long id;

    @Column(name = "generation")
    private String generation;
    @Column(name = "beginYear")
    private Integer beginYear;
    @Column(name = "endYear")
    private Integer endYear;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    //TODO checker coincidence


    public Generation() {
    }

    public Generation(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Integer getBeginYear() {
        return beginYear;
    }

    public void setBeginYear(int beginYear) {
        this.beginYear = beginYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
