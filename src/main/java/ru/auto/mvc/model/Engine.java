package ru.auto.mvc.model;

import ru.auto.mvc.model.enums.Fuel;
import ru.auto.mvc.model.enums.TypeOfEngine;

import javax.persistence.*;

/**
 * Created by EduardL on 24.04.15.
 */
@Entity(name = "engine")
@Table(name = "engine")
public class Engine {
    @Id
    @SequenceGenerator(name = "EngineIdSequence", sequenceName = "Engine_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EngineIdSequence")
    private Long id;

    @Column(name = "engine")
    private String engine;
    @Column(name = "capacity")
    private Double capacity;
    @Enumerated(EnumType.STRING)
    private Fuel fuel;
    @Enumerated(EnumType.STRING)
    private TypeOfEngine typeOfEngine;

    public Engine() {
    }

    public Engine(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public TypeOfEngine getTypeOfEngine() {
        return typeOfEngine;
    }

    public void setTypeOfEngine(TypeOfEngine typeOfEngine) {
        this.typeOfEngine = typeOfEngine;
    }
}
