package ru.auto.mvc.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import ru.auto.mvc.model.enums.Role;

import javax.persistence.*;
import java.util.List;

@Entity(name = "user_table")
@Table(name = "user_table")
public class User {

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    @Id
    @SequenceGenerator(name = "UserIdSequence", sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserIdSequence")
    private Long id;
    private String login;
    private String password;
    private String email;
    private Integer experience;
    private Double karma;
    @Enumerated(EnumType.STRING)
    private Role role;
    @OneToOne
    @JoinColumn(name = "profile_id")
    private UserProfile profile;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(uniqueConstraints= {@UniqueConstraint(columnNames={"user_id","auto_list_id"})}, name = "auto_list",
            joinColumns =  {@JoinColumn(name = "user_id")}, inverseJoinColumns = @JoinColumn(name = "auto_list_id"))
    private List<Auto> autoList;
    private String salt;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Double getKarma() {
        return karma;
    }

    public void setKarma(Double karma) {
        this.karma = karma;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public List<Auto> getAuto() {
        return autoList;
    }

    public void setAuto(List<Auto> autoList) {
        this.autoList = autoList;
    }
}
