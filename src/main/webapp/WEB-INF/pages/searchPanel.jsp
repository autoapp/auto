<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
    .widthParam {
        width: 15%;
    }
</style>

<form:form modelAttribute="auto" commandName="auto" action="/search" method="POST">
    <div class="navbar navbar-default">
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav" style="width: 100%">
                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle brand"
                       data-toggle="dropdown" id="0">Марка
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="brandCur" id="0"><a href="javascript:void(0)">Марка</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="brand" items="${allBrands}">
                            <li class="brandCur" id="${brand.id}"><a href="javascript:void(0)">${brand.brand}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle model"
                       data-toggle="dropdown" id="0">Модель
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="modelCur" id="0"><a href="javascript:void(0)">Модель</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="model" items="${allModels}">
                            <li class="modelCur" id="${model.id}"><a href="javascript:void(0)">${model.model}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle generation"
                       data-toggle="dropdown" id="0">Поколение
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="generationCur" id="0"><a
                                href="javascript:void(0)">Поколение</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="generation" items="${allGenerations}">
                            <li class="generationCur" id="${generation.id}" beginYear="${generation.beginYear}" endYear="${generation.endYear}"><a
                                    href="javascript:void(0)">${generation.generation}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <div class="col-lg-2">
                    <input class="form-control" placeholder="Год начальный" id="beginYear">
                </div>

                <div class="col-lg-2" style="margin-right: 3%">
                    <input class="form-control" placeholder="Год конечный" id="endYear">
                </div>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle engine"
                       data-toggle="dropdown" id="0">Двигатель
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="engineCur" id="0"><a
                                href="javascript:void(0)">Двигатель</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="engine" items="${allEngines}">
                            <li class="engineCur" id="${engine.id}"><a
                                    href="javascript:void(0)">${engine.engine}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav" style="width: 100%">
                <li class="dropdown" style="width: 12%">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle typeOfEngine"
                       data-toggle="dropdown" id="0">Тип двигателя
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="typeOfEngineCur" id="0"><a
                                href="javascript:void(0)">Тип двигателя</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="typeOfEngine" items="${allTypeOfEngines}">
                            <li class="typeOfEngineCur" id="${typeOfEngine}"><a
                                    href="javascript:void(0)">${typeOfEngine.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>


                <div class="col-lg-2">
                    <input class="form-control" placeholder="Объем начальный" id="beginCapacity">
                </div>

                <div class="col-lg-2" style="margin-right: 3%">
                    <input class="form-control" placeholder="Объем конечный" id="endCapacity">
                </div>

                <li class="dropdown" style="width: 12%">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle fuel"
                       data-toggle="dropdown" id="0">Топливо
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="fuelCur" id="0"><a href="javascript:void(0)">Топливо</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="fuel" items="${allFuels}">
                            <li class="fuelCur" id="${fuel}"><a href="javascript:void(0)">${fuel.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown" style="width: 12%">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle drivetrain"
                       data-toggle="dropdown" id="0">Привод
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="drivetrainCur" id="0"><a
                                href="javascript:void(0)">Привод</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="drivetrain" items="${allDrivetrains}">
                            <li class="drivetrainCur" id="${drivetrain}"><a
                                    href="javascript:void(0)">${drivetrain.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown" style="width: 12%">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle transmission"
                       data-toggle="dropdown" id="0">КПП
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="transmissionCur" id="0"><a
                                href="javascript:void(0)">КПП</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="transmission" items="${allTransmissions}">
                            <li class="transmissionCur" id="${transmission}"><a
                                    href="javascript:void(0)">${transmission.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle body"
                       data-toggle="dropdown" id="0">Кузов
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="bodyCur" id="0"><a href="javascript:void(0)">Кузов</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="body" items="${allBodys}">
                            <li class="bodyCur" id="${body}"><a href="javascript:void(0)">${body.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>

        </div>
        <a href="javascript:void(0)" class="btn btn-primary btn-raised butSearch">Найти</a>
    </div>
</form:form>