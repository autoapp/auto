<%--
  Created by IntelliJ IDEA.
  User: EduardL
  Date: 10.05.15
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>

    <style>
        .imageCar {

        }

        .curCar {
            margin-bottom: 3%;
        }

        .contCurCar {

        }

        img {
            width: 100%;
        }

        .col-lg-3 {
            width: 25%;
            height: 27%;
        }
    </style>
</head>
<body>

<c:forEach var="curCar" items="${searchedCars}">
    <div id="${curCar.id}" class="curCar col-lg-12">
        <div class="imageCar col-lg-3">
            <img
                    src="https://cs7052.vk.me/c7007/v7007545/39fb1/mfCmSCltQEA.jpg" height="185px">
        </div>
        <div>
            <a href="/auto/${curCar.id}"><p>${curCar.brand.brand}&nbsp;${curCar.model.model}(${curCar.generation.generation})&nbsp;${curCar.generation.beginYear}-${curCar.generation.endYear}</p></a>
            <p>Объем двигателя:&nbsp;${curCar.engine.capacity},&nbsp;Тип двигателя:&nbsp;${curCar.engine.typeOfEngine},&nbsp;Топливо:&nbsp;${curCar.engine.fuel}</p>
            <p>КПП:&nbsp;${curCar.transmission}, Привод:&nbsp;${curCar.drivetrain}</p>
            <c:if test="${curCar.euroNCAP != null}">
                <p>Безопасность(EURONCAP):&nbsp;${curCar.euroNCAP}</p>
            </c:if>
        </div>
    </div>
</c:forEach>

</body>
</html>
