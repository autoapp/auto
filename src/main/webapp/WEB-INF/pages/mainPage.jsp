<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template title="Главная">
<jsp:attribute name="headCSS">

</jsp:attribute>
<jsp:attribute name="headJS">
<script type="text/javascript" src="/resources/js/searchPanelScript.js"></script>
</jsp:attribute>
<jsp:attribute name="body">
    <div class="navPanel">
        <jsp:include page="searchPanel.jsp"></jsp:include>
    </div>
    <div class="currcarr navPanel">

    </div>
</jsp:attribute>
</t:template>

