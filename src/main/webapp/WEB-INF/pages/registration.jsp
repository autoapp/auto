<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Registration page</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/roboto.min.css" rel="stylesheet">
    <link href="/resources/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/resources/css/ripples.min.css" rel="stylesheet">
    <link href="/resources/css/registration.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
    <script type="text/javascript" src="/resources/js/ripples.min.js"></script>
    <script type="text/javascript" src="/resources/js/material.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/registration.js"></script>

    <style>
        .vertical-center {
            min-height: 100%; /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;

        }

        .horizontal-center {
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>
<body>
<script type="text/javascript">$(function () {
    $.material.init();
});</script>

<div class="container">
    <div class="row vertical-center">
        <div class="well bs-component horizontal-center col-lg-5">

            <form:form method="POST" action="/addUser" modelAttribute="UserRegistrationForm">
                <fieldset>
                    <legend>Регистрация</legend>

                    <div class="progress">
                        <div class="progress-bar progress-bar-info" style="width: 33%"></div>
                    </div>

                    <div class="first_step">
                        <div class="col-lg-12">
                            <form:input type="text" class="form-control floating-label reg-input" path="login" 
                                   id="login"
                                   placeholder="Логин"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="email" class="form-control floating-label reg-input" path="email"
                                   id="email"
                                   placeholder="E-mail"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="password" class="form-control floating-label reg-input" path="password"
                                   id="password" placeholder="Пароль"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="password" class="form-control floating-label reg-input" path="confirmPassword"
                                   id="confirmPassword" placeholder="Подтвердите пароль"/>
                        </div>

                        <div class="col-lg-3 col-lg-offset-8" style="margin-top: 10px">
                            <button class="btn btn-primary" id="first_step">
                                Далее
                            </button>
                        </div>
                    </div>
                    <div class="second_step" hidden="hidden">
                        <div class="col-lg-12">
                            <form:input type="text" class="form-control floating-label reg-input" path="name" 
                                   id="name"
                                   placeholder="Имя"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="text" class="form-control floating-label reg-input" path="surname" 
                                   id="surname"
                                   placeholder="Фамилия"/>
                        </div>

                        <div class="col-lg-12">
                            <div class="radio radio-primary reg-input">
                                <label>
                                    <form:radiobutton path="gender" value="мужской" checked="" />
                                    М
                                </label>
                                <label>
                                    <form:radiobutton path="gender" value="женский" />
                                    Ж
                                </label>
                            </div>
                        </div>

                        <label for="birth-date">День рождения:</label>
                        <div class="col-lg-12">
                            <form:input type="date" class="form-control reg-input" path="birthDate"
                                        id="birth-date"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="number" class="form-control floating-label reg-input" path="experience"
                                        min="0" max="80"
                                        id="experience"
                                        placeholder="Стаж вождения"/>
                        </div>

                        <div class="col-lg-12">
                            <form:input type="text" class="form-control floating-label reg-input" path="location"
                                        id="location"
                                        placeholder="Город"/>
                        </div>

                        <div class="col-lg-12">
                            <form:textarea type="text" class="form-control floating-label reg-input" path="summary"
                                        id="summary"
                                        placeholder="Обо мне"/>
                        </div>

                        <div class="col-lg-3 col-lg-offset-8" style="margin-top: 10px">
                            <button class="btn btn-primary" id="second_step">
                                Далее
                            </button>
                        </div>
                    </div>
                    <div class="fird_step" hidden="hidden">
                        <h2>Тут будет страница добавления автомобилей.</h2>

                        <div class="col-lg-7 col-lg-offset-4" style="margin-top: 10px">
                            <button name="submit" type="submit" class="btn btn-primary" id="fird_step">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>