<%--
  Created by IntelliJ IDEA.
  User: EduardL
  Date: 13.05.15
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Автомобиль">
    <jsp:attribute name="headJS">
<script type="text/javascript" src="/resources/js/currentCar.js"></script>
</jsp:attribute>
    <jsp:attribute name="body">

<div class="top container-fluid">
    <div class="container">
        <div class="shadow-z-2" style="background: white;min-height: 1000px;padding: 3%;">
            <div style="width: 501px; float: left">
                <img src="https://cs7052.vk.me/c7007/v7007545/39fb1/mfCmSCltQEA.jpg" height="340px" width="500px">
            </div>
            <div style="margin-left: 521px; position: relative">
                <a href="/auto/${currentCar.id}">
                    <p>${currentCar.brand.brand}&nbsp;${currentCar.model.model}(${currentCar.generation.generation})&nbsp;${currentCar.generation.beginYear}-${currentCar.generation.endYear}</p>
                </a>

                <p>Объем двигателя:&nbsp;${currentCar.engine.capacity},&nbsp;Тип
                    двигателя:&nbsp;${currentCar.engine.typeOfEngine.value},&nbsp;Топливо:&nbsp;${currentCar.engine.fuel.value}</p>

                <p>КПП:&nbsp;${currentCar.transmission.value}, Привод:&nbsp;${currentCar.drivetrain.value}</p>
                <c:if test="${currentCar.euroNCAP != null}">
                    <p>Безопасность(EURONCAP):&nbsp;${currentCar.euroNCAP}</p>
                </c:if>
            </div>

            <div style="margin-top: 275px; position: absolute; margin-left: 110px">
                <c:forEach var="curReview" items="${listReviews}">
                    <div style="float: left">
                        <div>
                            <img src="https://cs7052.vk.me/c540106/v540106245/213cb/T7qSHY1Hpbc.jpg" height="200px"
                                 width="200px">
                        </div>

                        <div align="center">
                                ${curReview.user.login}<br/>
                                ${curReview.user.karma}/10
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <p><b>Двигатель</b>(Оценка&nbsp;${curReview.motorRate}/10)<br/>
                                ${curReview.motorText}
                        </p>

                        <p><b>КПП</b>(Оценка&nbsp;${curReview.gearboxRate}/10)<br/>
                                ${curReview.gearboxText}
                        </p>

                        <p><b>Электроника</b>(Оценка&nbsp;${curReview.electronicRate}/10)<br/>
                                ${curReview.electronicText}
                        </p>

                        <p><b>Кузов</b>(Оценка&nbsp;${curReview.carcassRate}/10)<br/>
                                ${curReview.carcassText}
                        </p>

                        <p><b>Подвеска</b>(Оценка&nbsp;${curReview.suspentionRate}/10)<br/>
                                ${curReview.suspentionText}
                        </p>

                        <p><b>Плюсы</b><br/>
                                ${curReview.pros}
                        </p>

                        <p><b>Минусы</b><br/>
                                ${curReview.cons}
                        </p>

                        <p><b>Рейтинг удовлетворенности</b>(${curReview.expRate}/10)<br/></p>

                        <div class="icon-preview" style="float: right">
                            <i class="mdi-content-add">
                                ${curReview.plus}</i>&nbsp;
                            <i class="mdi-content-remove">
                                ${curReview.minus}</i>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
    </jsp:attribute>
</t:template>