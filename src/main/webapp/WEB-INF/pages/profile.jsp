<%--
  Created by IntelliJ IDEA.
  User: Rigen
  Date: 02.05.15
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<t:template title="Профиль">
<jsp:attribute name="headCSS">

</jsp:attribute>
<jsp:attribute name="headJS">
<script type="text/javascript" src="/resources/js/profile.js"></script>
<script type="text/javascript" src="/resources/js/jquery.barrating.min.js"></script>

</jsp:attribute>
<jsp:attribute name="body">
    <div class="container-fluid">
    <div class="container top horizontal-center profile-container">

    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Brand</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
                <li><a href="javascript:void(0)">Active</a></li>
                <li><a href="javascript:void(0)">Link</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="isAuthenticated()">
                    <li><a href="/profile" class="addButton"> Профиль </a></li>
                    <li><a href="/logout" class="addButton"> Выйти </a></li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li><a href="/login" class="addButton"> Войти </a></li>
                    <li><a href="/signup" class="addButton"> Зарегистрироваться </a></li>
                </security:authorize>
            </ul>
        </div>
    </div>

    <div class="shadow-z-2 topArea" style="background: white">
    <div class="some-container">
    <div class="row">
        <div class="profile-image col-lg-4" align="center">
            <img src="/resources/image/noavatar.png" class="avatar">

            <p class="karma">${user.karma}/10</p>
        </div>
        <div class="col-lg-8 profile-top">
            <p><strong id="login">${user.login}</strong>, ${user.role.value}</p>
            <c:if test="${(userProfile.name != null) || (userProfile.surname != null)}">
                <p>
                    <c:if test="${userProfile.name != null}">
                        ${userProfile.name}&nbsp;
                    </c:if>
                    <c:if test="${userProfile.surname != null}">
                        ${userProfile.surname}
                    </c:if>
                </p>
            </c:if>
            <c:if test="${userProfile.location != null}">
                <p>${userProfile.location}</p>
            </c:if>
            <c:if test="${userProfile.emailAccess}">
                <p>${user.email}</p>
            </c:if>
            <c:if test="${userProfile.birthDate != null}">
                <p>${userProfile.birthDate}, ? лет</p>
            </c:if>
        </div>
    </div>
    <c:if test="${userProfile.summary != null}">
        <div class="row profile-row">
            <label for="summary">Обо мне:</label>

            <p id="summary">${userProfile.summary}</p>
        </div>
    </c:if>
    <div class="row profile-row">
        <label for="auto">Мой бокс:</label>

        <div id="auto">
            <security:authorize access="isAuthenticated()">
                <div class="col-lg-3">
                    <div class="icon-preview" id="add-auto"><i class="mdi-content-add icon"></i><span>Добавить автомобиль</span>
                    </div>
                </div>
            </security:authorize>
            <div class="auto-list">
                <c:forEach var="auto" items="${autoList}">
                    <div class="col-lg-3 auto${auto.id}" align="center">
                        <a href="/auto/${auto.id}"><img src="${auto.images.get(0).link}" width="200" height="110"></a>

                        <p>${auto.brand.brand}&nbsp;${auto.model.model}
                            <c:if test="${reviewMap.get(auto.id) != null && reviewMap.get(auto.id).size() > 0 && reviewMap.get(auto.id).get(0).year != null}">
                                , ${reviewMap.get(auto.id).get(0).year}
                            </c:if>
                        </p>
                    </div>
                </c:forEach>
            </div>
        </div>

    </div>
    <div class="new-auto profile-row" hidden="hidden">
        <p class="profile-dialog">Какой у вас автомобиль?</p>

        <div class="review-row col-lg-12">
            <jsp:include page="autoSelecter.jsp"></jsp:include>
        </div>
        <div class="review-row review-button">
            <div class="review-row review-button errorAuto">
            </div>
            <button class="btn btn-default add-new-review">Оставить отзыв</button>
            <button class="btn btn-primary add-auto-submit">Добавить авто</button>
        </div>
        <div class="new-review" hidden="hidden">

            <div class="review-row">
                <p class="profile-dialog">Почему вы купили это авто?</p>
                <textarea class="form-control" id="reason"
                          placeholder="Расскажите, с какой целью Вы приобрели авто."></textarea></div>

            <div class="review-row" id="engine">
                <p class="profile-dialog">Двигатель</p>
                <textarea class="form-control"
                          placeholder="Опишите, какие проблемы были с двигателем, как дорого его обслуживать."></textarea>

                <p><strong>Оцените двигатель</strong></p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>

            <div class="review-row" id="gearbox">
                <p class="profile-dialog">Коробка переключения передач</p>
                <textarea class="form-control"
                          placeholder="Опишите КПП. Много ли с ним хлопот? Как дорога в эксплуатации?"></textarea>

                <p><strong>Оцените КПП</strong></p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>

            <div class="review-row" id="electr">
                <p class="profile-dialog">Электроника</p>
                <textarea class="form-control"
                          placeholder='Часто ли "????"? Приходилось ли ремонтировать? Как дорого?'></textarea>

                <p><strong>Оцените электронику</strong></p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>

            <div class="review-row" id="body">
                <p class="profile-dialog">Кузов</p>
                <textarea class="form-control"
                          placeholder="Подвергался ли коррозии? Дорого ли ремонтировать кузов, если пришлось?"></textarea>

                <p><strong>Оцените кузов</strong></p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>

            <div class="review-row" id="suspention">
                <p class="profile-dialog">Подвеска</p>
                <textarea class="form-control"
                          placeholder='Часто ли приходится "перетряхивать"? Дорогие ли запчасти?'></textarea>

                <p><strong>Оцените подвеску</strong></p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>

            <div class="review-row" id="pros">
                <p class="profile-dialog">Какие плюсы у авто?</p>
                <input class="form-control" placeholder='Плюсы'>
            </div>

            <div class="review-row" id="cons">
                <p class="profile-dialog">Какие минусы у авто?</p>
                <input class="form-control" placeholder='Минусы'>
            </div>

            <div class="review-row" id="exp">
                <p class="profile-dialog">Насколько Вы довольны своим авто?</p>

                <div class="br-wrapper">
                    <select class="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary add-auto-review review-button">Добавить автомобиль и отзыв
            </button>
        </div>
    </div>
    <input hidden="hidden" name='_csrf' value="${_csrf.token}">
    </div>
    </div>
    </div>
    </div>

</jsp:attribute>
</t:template>