<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
    .widthParam {
        width: 16%;
    }
</style>

    <div class="navbar navbar-default">
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav" style="width: 100%">
                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle brandSelect"
                       data-toggle="dropdown" id="0">Марка
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="brandCurSelect" id="0"><a href="javascript:void(0)">Марка</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="brand" items="${allBrands}">
                            <li class="brandCurSelect" id="${brand.id}"><a href="javascript:void(0)">${brand.brand}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle modelSelect"
                       data-toggle="dropdown" id="0">Модель
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="modelCurSelect" id="0"><a href="javascript:void(0)">Модель</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="model" items="${allModels}">
                            <li class="modelCurSelect" id="${model.id}"><a href="javascript:void(0)">${model.model}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle generationSelect"
                       data-toggle="dropdown" id="0">Поколение
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="generationCurSelect" id="0"><a
                                href="javascript:void(0)">Поколение</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="generation" items="${allGenerations}">
                            <li class="generationCurSelect" id="${generation.id}"><a
                                    href="javascript:void(0)">${generation.generation}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <div class="col-lg-2">
                    <input type="number" min="1900" max="2015" class="form-control" placeholder="Год выпуска" id="beginYearSelect">
                </div>

                <div class="col-lg-2">
                    <input class="form-control" type="number" min="0" max="80" placeholder="Стаж владения" id="expYearSelect">
                </div>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle engineSelect"
                       data-toggle="dropdown" id="0">Двигатель
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="engineCurSelect" id="0"><a
                                href="javascript:void(0)">Двигатель</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="engine" items="${allEngines}">
                            <li class="engineCurSelect" id="${engine.id}"><a
                                    href="javascript:void(0)">${engine.engine}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav" style="width: 100%">
                <li class="dropdown" style="width: 17%">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle typeOfEngineSelect"
                       data-toggle="dropdown" id="0">Тип двигателя
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="typeOfEngineCurSelect" id="0"><a
                                href="javascript:void(0)">Тип двигателя</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="typeOfEngine" items="${allTypeOfEngines}">
                            <li class="typeOfEngineCurSelect" id="${typeOfEngine}"><a
                                    href="javascript:void(0)">${typeOfEngine.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>


                <div class="col-lg-2">
                    <input type="number" min="0.8" max="7" step="0.1" class="form-control" placeholder="Объем" id="beginCapacitySelect">
                </div>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle fuelSelect"
                       data-toggle="dropdown" id="0">Топливо
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="fuelCurSelect" id="0"><a href="javascript:void(0)">Топливо</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="fuel" items="${allFuels}">
                            <li class="fuelCurSelect" id="${fuel}"><a href="javascript:void(0)">${fuel.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle drivetrainSelect"
                       data-toggle="dropdown" id="0">Привод
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="drivetrainCurSelect" id="0"><a
                                href="javascript:void(0)">Привод</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="drivetrain" items="${allDrivetrains}">
                            <li class="drivetrainCurSelect" id="${drivetrain}"><a
                                    href="javascript:void(0)">${drivetrain.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle transmissionSelect"
                       data-toggle="dropdown" id="0">КПП
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="transmissionCurSelect" id="0"><a
                                href="javascript:void(0)">КПП</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="transmission" items="${allTransmissions}">
                            <li class="transmissionCurSelect" id="${transmission}"><a
                                    href="javascript:void(0)">${transmission.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown widthParam">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle bodySelect"
                       data-toggle="dropdown" id="0">Кузов
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="bodyCurSelect" id="0"><a href="javascript:void(0)">Кузов</a></li>
                        <li role="presentation" class="divider"></li>
                        <c:forEach var="body" items="${allBodys}">
                            <li class="bodyCurSelect" id="${body}"><a href="javascript:void(0)">${body.value}</a></li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>
        </div>
    </div>