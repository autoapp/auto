<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/roboto.min.css" rel="stylesheet">
    <link href="/resources/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/resources/css/ripples.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
    <script type="text/javascript" src="/resources/js/ripples.min.js"></script>
    <script type="text/javascript" src="/resources/js/material.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <style>
        .vertical-center {
            min-height: 100%; /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;

        }

        .horizontal-center {
            margin-left: auto;
            margin-right: auto;
        }
    </style>

</head>

<script type="text/javascript">$(function () {
    $.material.init();
});</script>
<body>
<c:if test="${not empty msg}">
    <div class="msg">${msg}</div>
</c:if>
<div class="container-fluid">
    <div class="row vertical-center">
        <div class="well bs-component horizontal-center col-lg-3 ">
            <form class="form-horizontal" name='loginForm'
                  action="<c:url value='/j_spring_security_check' />" method='POST'>
                <fieldset>
                    <legend>Авторизация</legend>
                    <c:if test="${not empty error}"><p class="text-danger">Error user or password!</p></c:if>
                    <div class="form-group <c:if test="${not empty error}">has-error</c:if>">
                        <div class="col-lg-12">
                            <input type='text' class="form-control" id="inputName" name='username'
                                   placeholder="Логин">
                        </div>
                    </div>

                    <div class="form-group <c:if test="${not empty error}">has-error</c:if>">
                        <div class="col-lg-12">
                            <input type="password" class="form-control" name='password' id="inputPassword"
                                   placeholder="Пароль">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-19 col-lg-offset-3">
                            <button name="submit" type="submit" value="submit" class="btn btn-primary">Войти</button>
                            <a href="/signup" class="btn btn-default"> Регистрация </a>
                        </div>
                    </div>

                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>