;
$(document).ready(function () {
    var csrf = $('input[name=_csrf]').val();

    $('.rating').barrating().barrating('show', {
        wrapperClass: 'br-wrapper-g',
        showSelectedRating: true
    });
    $('#add-auto').click(function () {
        $('.new-auto').slideToggle('normal');
    });
    $('.add-new-review').click(function () {
        $('.new-review').slideToggle('normal');
    });

    function resetSelectForm() {
        $('.dropdown-toggle.brandSelect').html($('.brandCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.modelSelect').html($('.modelCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.generationSelect').html($('.generationCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.typeOfEngineSelect').html($('.typeOfEngineCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.engineSelect').html($('.engineCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.fuelSelect').html($('.fuelCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.drivetrainSelect').html($('.drivetrainCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.transmissionSelect').html($('.transmissionCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('.dropdown-toggle.bodySelect').html($('.bodyCurSelect[id=0]').text() + " <b class='caret'></b>").attr('id', 0);
        $('#beginYearSelect').val('');
        $('#expYearSelect').val('');
        $('#beginCapacitySelect').val('');
    }

    $('.add-auto-submit, .add-auto-review').on('click', function () {
        var brand = $('.dropdown-toggle.brandSelect').attr('id');
        var model = $('.dropdown-toggle.modelSelect').attr('id');
        var generation = $('.dropdown-toggle.generationSelect').attr('id');
        var typeOfEngine = $('.dropdown-toggle.typeOfEngineSelect').attr('id');
        var engine = $('.dropdown-toggle.engineSelect').attr('id');
        var fuel = $('.dropdown-toggle.fuelSelect').attr('id');
        var beginYear = $('#beginYearSelect').val();
        if (beginYear == "") {
            beginYear = 0;
        }
        var expYear = $('#expYearSelect').val();
        if (expYear == "") {
            expYear = null;
        }
        var beginCapacity = $('#beginCapacitySelect').val();

        if (beginCapacity == "") {
            beginCapacity = 0;
        }
        var drivetrain = $('.dropdown-toggle.drivetrainSelect').attr('id');
        var transmission = $('.dropdown-toggle.transmissionSelect').attr('id');
        var body = $('.dropdown-toggle.bodySelect').attr('id');
        var user = $('#login').text();
        var button = this;


        $.ajax({
            type: "POST",
            url: "/addAuto",
            data: {
                "idBrand": brand, "idModel": model, "idGeneration": generation,
                "beginYear": beginYear, "endYear": beginYear, "typeOfEngine": typeOfEngine, "idEngine": engine,
                "beginCapacity": beginCapacity, "endCapacity": beginCapacity, "fuel": fuel,
                "drivetrain": drivetrain, "transmission": transmission, "body": body, "_csrf": csrf
            },
            success: function (data) {
//                var auto = $.parseJSON(data + "");
                $('.auto-list').prepend(
                    '<div class="col-lg-3 auto'+ $(data).attr("id") + '" align="center">' +
                    '<a href="/auto/' + $(data).attr("id") + '"><img src="' + $($(data).attr("images")[0]).attr("link") + '" width="200" height="110"></a>' +
                    '<p>' + $($(data).attr("brand")).attr("brand") + '&nbsp;' + $($(data).attr("model")).attr("model") +
                    '</p>' +
                    '</div>');
                if (data != null && (beginYear != 0 || expYear != null)) {
                    if (beginYear == 0) {
                        beginYear = null;
                    }
                    var reviewData;
                    if ($(button).hasClass('add-auto-review')) {
                        reviewData = {
                            "autoId": $(data).attr("id"), "year": beginYear,
                            "experience": expYear, "pros": $('#pros input').val(),
                            "cons": $('#cons input').val(), "impression": $('#reason').val(), "plus": 0,
                            "minus": 0, "expRate": $('#exp .br-current-rating').text(),
                            "motorText": $('#engine textarea').val(), "motorRate": $('#engine .br-current-rating').text(),
                            "gearboxText": $('#gearbox textarea').val(), "gearboxRate": $('#gearbox .br-current-rating').text(),
                            "electronicText": $('#electr textarea').val(), "electronicRate": $('#electr .br-current-rating').text(),
                            "carcassText": $('#body textarea').val(), "carcassRate": $('#body .br-current-rating').text(),
                            "suspentionText": $('#suspention textarea').val(), "suspentionRate": $('#suspention .br-current-rating').text(),
                            "_csrf": csrf
                        }
                    } else {
                        reviewData = {
                            "autoId": $(data).attr("id"), "year": beginYear,
                            "experience": expYear, "userName": user, "_csrf": csrf
                        }
                    }
                    $.ajax({
                        type: "POST",
                        url: "/addReview",
                        data: reviewData,
                        success: function (data) {
                            resetSelectForm();
                            $('.new-auto').slideToggle('normal');
                            if (beginYear != null) {
                                $('.auto-list p').append(", " + beginYear);
                            }
                        },
                        error: function () {
                            console.log();
                        }
                    });
                } else {
                    resetSelectForm();
                    $('.new-auto').slideToggle('normal');
                }
            },
            error: function () {
                $('.errorAuto').html('<strong class="text-danger">Ошибка добавления!</strong>');
            }
        });
    });

    $('.add-auto-review2').on('click', function () {

        var brand = $('.dropdown-toggle.brandSelect').attr('id');
        var model = $('.dropdown-toggle.modelSelect').attr('id');
        var generation = $('.dropdown-toggle.generationSelect').attr('id');
        var typeOfEngine = $('.dropdown-toggle.typeOfEngineSelect').attr('id');
        var engine = $('.dropdown-toggle.engineSelect').attr('id');
        var fuel = $('.dropdown-toggle.fuelSelect').attr('id');
        var beginYear = $('#beginYearSelect').val();
        if (beginYear == "") {
            beginYear = null;
        }
        var expYear = $('#expYearSelect').val();
        if (expYear == "") {
            expYear = null;
        }
        var beginCapacity = $('#beginCapacitySelect').val();
        if (beginCapacity == "") {
            beginCapacity = null;
        }
        var drivetrain = $('.dropdown-toggle.drivetrainSelect').attr('id');
        var transmission = $('.dropdown-toggle.transmissionSelect').attr('id');
        var body = $('.dropdown-toggle.bodySelect').attr('id');


        var data;
        $.ajax({
            type: "POST",
            url: "/addReview",
            data: {"autoId": data, "pros": $('#pros input').text(),
                "cons": $('#cons input').text(), "impression": $('#reason').val(), "plus": 0,
                "minus": 0, "expRate": $('#exp .br-current-rating').text(),
                "motorText": $('#engine textarea').val(), "motorRate": $('#engine .br-current-rating').text(),
                "gearboxText": $('#gearbox textarea').val(), "gearboxRate": $('#gearbox .br-current-rating').text(),
                "electronicText": $('#electr textarea').val(), "electronicRate": $('#electr .br-current-rating').text(),
                "carcassText": $('#body textarea').val(), "carcassRate": $('#body .br-current-rating').text(),
                "suspentionText": $('#suspention textarea').val(), "suspentionRate": $('#suspention .br-current-rating').text(),
                "year": $("???"), "experience": $('???'), "_csrf": csrf
            },
            success: function (data) {
            },
            error: function () {
                console.log();
            }
        });
    });

    $('.brandCurSelect').on('click', function () {
        var brandCurSelect = $(this).text();
        var idBrandCurSelect = $(this).attr('id');
        $('.dropdown-toggle.brandSelect').html(brandCurSelect + " <b class='caret'></b>").attr('id', idBrandCurSelect);
    });

    $('.modelCurSelect').on('click', function () {
        var modelCurSelect = $(this).text();
        var idModelCurSelect = $(this).attr('id');
        $('.dropdown-toggle.modelSelect').html(modelCurSelect + " <b class='caret'></b>").attr('id', idModelCurSelect);
    });

    $('.generationCurSelect').on('click', function () {
        var generationCurSelect = $(this).text();
        var idGenerationCurSelect = $(this).attr('id');
        $('.dropdown-toggle.generationSelect').html(generationCurSelect + " <b class='caret'></b>").attr('id', idGenerationCurSelect);
    });

    $('.typeOfEngineCurSelect').on('click', function () {
        var typeOfEngineCurSelect = $(this).text();
        var idTypeOfEngineCurSelect = $(this).attr('id');
        $('.dropdown-toggle.typeOfEngineSelect').html(typeOfEngineCurSelect + " <b class='caret'></b>").attr('id', idTypeOfEngineCurSelect);
    });

    $('.engineCurSelect').on('click', function () {
        var engineCurSelect = $(this).text();
        var idEngineCurSelect = $(this).attr('id');
        $('.dropdown-toggle.engineSelect').html(engineCurSelect + " <b class='caret'></b>").attr('id', idEngineCurSelect);
    });

    $('.fuelCurSelect').on('click', function () {
        var fuelCurSelect = $(this).text();
        var idFuelCurSelect = $(this).attr('id');
        $('.dropdown-toggle.fuelSelect').html(fuelCurSelect + " <b class='caret'></b>").attr('id', idFuelCurSelect);
    });

    $('.drivetrainCurSelect').on('click', function () {
        var drivetrainCurSelect = $(this).text();
        var idDrivetrainCurSelect = $(this).attr('id');
        $('.dropdown-toggle.drivetrainSelect').html(drivetrainCurSelect + " <b class='caret'></b>").attr('id', idDrivetrainCurSelect);
    });

    $('.transmissionCurSelect').on('click', function () {
        var transmissionCurSelect = $(this).text();
        var idTransmissionCurSelect = $(this).attr('id');
        $('.dropdown-toggle.transmissionSelect').html(transmissionCurSelect + " <b class='caret'></b>").attr('id', idTransmissionCurSelect);
    });

    $('.bodyCurSelect').on('click', function () {
        var bodyCurSelect = $(this).text();
        var idBodyCurSelect = $(this).attr('id');
        $('.dropdown-toggle.bodySelect').html(bodyCurSelect + " <b class='caret'></b>").attr('id', idBodyCurSelect);
    });


})
;