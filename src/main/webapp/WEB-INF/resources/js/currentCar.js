;
$(document).ready(function () {
    var csrf = $('input[name=_csrf]').val();

    $('.mdi-content-add').on('click', function () {
        var plusRating = $(this).html();
        $.ajax({
            type: "POST",
            url: "/plusReview",
            date: {"_csrf": csrf},
            success: function() {
                $(this).html("");
            }
        })
    });

})
;