/**
 * Created by EduardL on 30.04.15.
 */

;
$(document).ready(function () {
    var csrf = $('input[name=_csrf]').val();

    $('.brandCur').on('click', function () {
        var brandCur = $(this).text();
        var idBrandCur = $(this).attr('id');
        $('.dropdown-toggle.brand').html(brandCur + " <b class='caret'></b>").attr('id', idBrandCur);
    });

    $('.modelCur').on('click', function () {
        var modelCur = $(this).text();
        var idModelCur = $(this).attr('id');
        $('.dropdown-toggle.model').html(modelCur + " <b class='caret'></b>").attr('id', idModelCur);
    });

    $('.generationCur').on('click', function () {
        var generationCur = $(this).text();
        var idGenerationCur = $(this).attr('id');
        var beginYear = $(this).attr('beginYear');
        var endYear = $(this).attr('endYear');
        $('.dropdown-toggle.generation').html(generationCur + " <b class='caret'></b>").attr('id', idGenerationCur);
        $('#beginYear').val(beginYear);
        $('#endYear').val(endYear);
    });

    $('.typeOfEngineCur').on('click', function () {
        var typeOfEngineCur = $(this).text();
        var idTypeOfEngineCur = $(this).attr('id');
        $('.dropdown-toggle.typeOfEngine').html(typeOfEngineCur + " <b class='caret'></b>").attr('id', idTypeOfEngineCur);
    });

    $('.engineCur').on('click', function () {
        var engineCur = $(this).text();
        var idEngineCur = $(this).attr('id');
        $('.dropdown-toggle.engine').html(engineCur + " <b class='caret'></b>").attr('id', idEngineCur);
    });

    $('.fuelCur').on('click', function () {
        var fuelCur = $(this).text();
        var idFuelCur = $(this).attr('id');
        $('.dropdown-toggle.fuel').html(fuelCur + " <b class='caret'></b>").attr('id', idFuelCur);
    });

    $('.drivetrainCur').on('click', function () {
        var drivetrainCur = $(this).text();
        var idDrivetrainCur = $(this).attr('id');
        $('.dropdown-toggle.drivetrain').html(drivetrainCur + " <b class='caret'></b>").attr('id', idDrivetrainCur);
    });

    $('.transmissionCur').on('click', function () {
        var transmissionCur = $(this).text();
        var idTransmissionCur = $(this).attr('id');
        $('.dropdown-toggle.transmission').html(transmissionCur + " <b class='caret'></b>").attr('id', idTransmissionCur);
    });

    $('.bodyCur').on('click', function () {
        var bodyCur = $(this).text();
        var idBodyCur = $(this).attr('id');
        $('.dropdown-toggle.body').html(bodyCur + " <b class='caret'></b>").attr('id', idBodyCur);
    });

    $('.butSearch').on('click', function () {
        var brand = $('.dropdown-toggle.brand').attr('id');
        var model = $('.dropdown-toggle.model').attr('id');
        var generation = $('.dropdown-toggle.generation').attr('id');
        var typeOfEngine = $('.dropdown-toggle.typeOfEngine').attr('id');
        var engine = $('.dropdown-toggle.engine').attr('id');
        var fuel = $('.dropdown-toggle.fuel').attr('id');
        var beginYear = $('#beginYear').val();
        if (beginYear == "") {
            beginYear = 0;
        }
        ;
        var endYear = $('#endYear').val();
        if (endYear == "") {
            endYear = 0;
        }
        var beginCapacity = $('#beginCapacity').val();
        if (beginCapacity == "") {
            beginCapacity = 0;
        }
        var endCapacity = $('#endCapacity').val();
        if (endCapacity == "") {
            endCapacity = 0;
        }
        var drivetrain = $('.dropdown-toggle.drivetrain').attr('id');
        var transmission = $('.dropdown-toggle.transmission').attr('id');
        var body = $('.dropdown-toggle.body').attr('id');
        $.ajax({
            type: "POST",
            url: "/search",
            data: {"idBrand": brand, "idModel": model, "idGeneration": generation, "beginYear": beginYear, "endYear": endYear, "typeOfEngine": typeOfEngine,
                "idEngine": engine, "beginCapacity": beginCapacity, "endCapacity": endCapacity, "fuel": fuel, "drivetrain": drivetrain, "transmission": transmission, "body": body, "_csrf": csrf},
            success: function(data) {
                console.log("I'm here");
                $('.currcarr').html("");
                $('.currcarr').prepend(data);
            },
            error: function() {
                console.log();
            }
        });
    })
    ;
})
;